//
//  JMLongitudeLatitudeFormater.h
//  JMLongitudeLatitudeFormater
//
//  Created by Nicolas Cherel on 09/05/05.
//  Copyright 2005 Magic Instinct Software.
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#import <Cocoa/Cocoa.h>

#if defined _DEBUG
#define __MY_NSLog(PARAM, ...) NSLog(PARAM, __VA_ARGS__);
#else
#define __MY_NSLog(PARAM, ...)
#endif

@interface JMLongitudeLatitudeFormatter : NSFormatter <NSCopying, NSCoding>
{
	@private
	
	/*
	 *   Format properties
	 */
    BOOL _withMinutes;
    BOOL _withSeconds;
    BOOL _withLiteral;
    BOOL _longitude;
    BOOL _localized;
	
	/*
	 *   Formatters for degress, minutes and seconds textual representation
	 */ 
    NSNumberFormatter * _degreesFormatter;
    NSNumberFormatter * _minutesFormatter;
    NSNumberFormatter * _secondsFormatter;
	
	/*
	 *   Strings placed after textual value
	 */
    NSString * _descDegrees;
    NSString * _descMinutes;
    NSString * _descSeconds;
	
	/*
	 *   Custom dictionary for locales (only the decimal seprator is used)
	 */ 
    NSDictionary * _myLocale;
}

/*
 *  Accessors
 */
- (BOOL)withMinutes;
- (void)setWithMinutes:(BOOL)flag;
- (BOOL)withSeconds;
- (void)setWithSeconds:(BOOL)flag;
- (BOOL)withLiteral;
- (void)setWithLiteral:(BOOL)flag;
- (BOOL)longitude;
- (void)setLongitude:(BOOL)flag;
- (BOOL)localized;

/* Don't use these next fifteen accessors */
- (void)setLocalized:(BOOL)flag;
- (NSNumberFormatter *)degreesFormatter;
- (void)setDegreesFormatter:(NSNumberFormatter *)aDegreesFormatter;
- (NSNumberFormatter *)minutesFormatter;
- (void)setMinutesFormatter:(NSNumberFormatter *)aMinutesFormatter;
- (NSNumberFormatter *)secondsFormatter;
- (void)setSecondsFormatter:(NSNumberFormatter *)aSecondsFormatter;
- (NSString *)descDegrees;
- (void)setDescDegrees:(NSString *)aDescDegrees;
- (NSString *)descMinutes;
- (void)setDescMinutes:(NSString *)aDescMinutes;
- (NSString *)descSeconds;
- (void)setDescSeconds:(NSString *)aDescSeconds;
- (NSDictionary *)myLocale;
- (void)setMyLocale:(NSDictionary *)aMyLocale;

/* Set localisation for the decimal separator */
- (void)setLocalizesFormat:(BOOL)flag;

/* Set the format */
- (BOOL)setFormat:(NSString *)format;
/* Construct a displayable string for the format */
- (NSString *)format;

+ (NSString *)defaultFormat;
+ (JMLongitudeLatitudeFormatter *)longitudeLatitudeFormatterWithFormat:(NSString *)format;
@end
