//
//  JMLongitudeLatitudeFormater.m
//  JMLongitudeLatitudeFormater
//
//  Created by Nicolas Cherel on 09/05/05.
//  Copyright 2005 Magic Instinct Software.
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


#include <math.h>

#import "JMLongitudeLatitudeFormatter.h"

static NSString * __secondsDefaultFormat = nil;
static NSString * __minutesDefaultFormat = nil;
static NSString * __degreesDefaultFormat = nil;
static NSString * __defaultFormat = nil;

@implementation JMLongitudeLatitudeFormatter

+ (void)initialize
{
	if(self == [JMLongitudeLatitudeFormatter class]) {
        __secondsDefaultFormat = @"#0.##;0;#0.##";
        __minutesDefaultFormat = @"#0;#0;#0";
        __degreesDefaultFormat = @"#0;#0;#0";
        __defaultFormat = @"#0\xa1 #0\' #0.##\" ";
	}
}

+ (JMLongitudeLatitudeFormatter *)longitudeLatitudeFormatterWithFormat:(NSString *)format
{
    __MY_NSLog(@"%s", __FUNCTION__  )
    
	JMLongitudeLatitudeFormatter * instance = [[[self class] alloc] init];
	[instance setFormat:format];
	return [instance autorelease];
}


+ (NSString *)defaultFormat
{
	__MY_NSLog(@"%s", __FUNCTION__  )
	return __defaultFormat;
}



- (JMLongitudeLatitudeFormatter *)init
{
	__MY_NSLog(@"%s", __FUNCTION__  )
	self = [super init];
    if (self) {
        
        // default format initialization
        _degreesFormatter = [[NSNumberFormatter alloc] init];
        [_degreesFormatter setFormat:__degreesDefaultFormat];
        _descDegrees = [NSString stringWithString:@"\xa1 "]; // \xa1 for degree (°) symbol 
        [_descDegrees retain];
        
        _minutesFormatter = [[NSNumberFormatter alloc] init];
        [_minutesFormatter setFormat:__minutesDefaultFormat];
        _descMinutes = [NSString stringWithString:@"\' "];
        [_descMinutes retain];
        
        _secondsFormatter = [[NSNumberFormatter alloc] init];
        [_secondsFormatter setFormat:__secondsDefaultFormat];
        _descSeconds = [NSString stringWithString:@"\" "];
        [_descSeconds retain];
        
        // get locale's decimal separator and initialize the locale dictionnary (_myLocale for NSScanner and
        // setLocalizesFormat:flag
        id decimalSeparator = [[NSUserDefaults standardUserDefaults] objectForKey:NSDecimalSeparator];
        _myLocale = [NSDictionary dictionaryWithObject:decimalSeparator forKey:NSDecimalSeparator];
        [_myLocale retain];
    }
	return self;
}


- (void)setLocalizesFormat:(BOOL)flag
{
	__MY_NSLog(@"%s", __FUNCTION__  )
	if (flag) {
		[_degreesFormatter setDecimalSeparator:[_myLocale objectForKey:NSDecimalSeparator]];
		[_minutesFormatter setDecimalSeparator:[_myLocale objectForKey:NSDecimalSeparator]];
		[_secondsFormatter setDecimalSeparator:[_myLocale objectForKey:NSDecimalSeparator]];
	} else {
		// TODO : find a way to get the real default decimalSeparator instead of initialize it with 
		// the @"." string.
		[_degreesFormatter setDecimalSeparator:@"."];
		[_minutesFormatter setDecimalSeparator:@"."];
		[_secondsFormatter setDecimalSeparator:@"."]; 
	}
	_localized = flag;
}

- (BOOL)setFormat:(NSString *)format
{
	__MY_NSLog(@"%s : %s", __FUNCTION__, [format cString])
    
	NSScanner * scanner = [NSScanner scannerWithString:format];
	NSCharacterSet * formatCharacters = [NSCharacterSet characterSetWithCharactersInString:@".#0_L"];
		
	NSString * degreesFormat = @"";
	NSString * minutesFormat = @"";
	NSString * secondsFormat = @"";
	
	[_descDegrees release];
	[_descMinutes release];
	[_descSeconds release];
	
	_descDegrees = @"";
	_descMinutes = @"";
	_descSeconds = @"";
	
	if ([scanner scanCharactersFromSet:formatCharacters intoString:&degreesFormat]) {
		if (! [scanner scanUpToCharactersFromSet:formatCharacters intoString:&_descDegrees]) _descDegrees = @"\xa1 ";
		[_descDegrees retain];
	} else {
		_descDegrees = @"\xa1 ";
		[_descDegrees retain];
	}
	
	if ([scanner scanString:@"L" intoString:nil]) {
		[self setWithMinutes:NO];
		[self setWithLiteral:YES];
	} else {
		if ([scanner scanCharactersFromSet:formatCharacters intoString:&minutesFormat]){
			if (! [scanner scanUpToCharactersFromSet:formatCharacters intoString:&_descMinutes]) _descMinutes = @"\' ";
			[self setWithMinutes:YES];
			[_descMinutes retain];
		}  else {
			[self setWithMinutes:NO];
			_descMinutes = @"\' ";
			[_descMinutes retain];
		}
		
		if ([scanner scanString:@"L" intoString:nil]) {
			[self setWithSeconds:NO];
			[self setWithLiteral:YES];
		} else {
			if ([scanner scanCharactersFromSet:formatCharacters intoString:&secondsFormat]){
				if (! [scanner scanUpToString:@"L" intoString:&_descSeconds]) _descSeconds = @"\" ";
				[self setWithSeconds:YES];
				[_descSeconds retain]; 
			} else {
				[self setWithSeconds:NO];
				_descSeconds = @"\" ";
				[_descSeconds retain];
			}
			
			if ([scanner scanString:@"L" intoString:nil]) {
				[self setWithLiteral:YES];
			} else {
				[self setWithLiteral:NO];
			}
		}
	}

    
	NSMutableString * formatTMP = [NSMutableString stringWithString:degreesFormat];
	
	if (degreesFormat == nil || [degreesFormat isEqualToString:@""]) {
		formatTMP = [NSMutableString stringWithString:__degreesDefaultFormat];
	}
	
	[_degreesFormatter setFormat:formatTMP];
	
	if (minutesFormat == nil || [minutesFormat isEqualToString:@""]) {
		formatTMP = [NSMutableString stringWithString:__minutesDefaultFormat];
	} else {
		formatTMP = [NSMutableString stringWithString:minutesFormat];
	}
	
	[_minutesFormatter setFormat:formatTMP];
	
	if (secondsFormat == nil || [secondsFormat isEqualToString:@""]) {
		formatTMP = [NSMutableString stringWithString:__secondsDefaultFormat];
	} else {
		formatTMP = [NSMutableString stringWithString:secondsFormat];
	}
    
	[_secondsFormatter setFormat:formatTMP];
	
	[self setLocalizesFormat:_localized];
    
	return YES;
}

- (NSString *)format
{
	NSString * minutesFormat = [[[_minutesFormatter format] componentsSeparatedByString:@";"] objectAtIndex:0];
	NSString * secondsFormat = [[[_secondsFormatter format] componentsSeparatedByString:@";"] objectAtIndex:0];
	NSString * degreesFormat = [[[_degreesFormatter format] componentsSeparatedByString:@";"] objectAtIndex:0];
	NSMutableString * ret = [NSMutableString stringWithString:degreesFormat];
	[ret appendString:_descDegrees];
	if (_withMinutes) {
		[ret appendString:minutesFormat];
		[ret appendString:_descMinutes];
		if (_withSeconds) {
			[ret appendString:secondsFormat];
			[ret appendString:_descSeconds];
		}
	}
	if (_withLiteral){
		[ret appendString:@"L"];
	}
	return ret;
}



- (NSString *)stringForObjectValue:(id)anObject {
    
	NSMutableString * ret = [[[NSMutableString alloc] init] autorelease];
    
	if ([anObject isKindOfClass:[NSNumber class]]){
		double value = [anObject doubleValue];
		
		BOOL negativeValue = value < .0;
		if (negativeValue) value = -value;
		
		if ( _longitude && (value > 180.0) ) return @"";
		if ( !_longitude && (value > 90.0) ) return @"";
		
		double degrees = 0;
		if (_withMinutes) {
			degrees = (int) value;
			double  roundedDegrees = round (value * 1.e10) / 1.e10;
			if ((roundedDegrees - degrees) > 0.9999999999) {
				degrees = roundedDegrees;
			}
		} else {
			degrees = value;
		}
		
		value -= degrees;
		value *= 60.;
		
		double minutes = 0;
		if (_withSeconds) {
			minutes = (int) value;
			double  roundedMinutes = round (value * 1.e10) / 1.e10;
			if ((roundedMinutes - minutes) > 0.9999999999) {
				minutes = roundedMinutes;
			}
		} else {
			minutes = value;
		}
        
		value -= minutes;
		
		value *= 60.;
		double seconds = value;
		
		// negative values for seconds are always due to minutes rounding, and very small,
		// we can ignore it.
		if (seconds < 0) seconds = 0;
        
		if ([[_secondsFormatter 
				stringForObjectValue:[NSNumber numberWithDouble:seconds]] doubleValue] == 60.)
		{
			seconds = 0.;
			minutes += 1.;
		}
		
		if ([[_minutesFormatter 
				stringForObjectValue:[NSNumber numberWithDouble:minutes]] doubleValue] == 60.)
		{					
			minutes = 0.;
			degrees += 1;
		}
		
		
		if (negativeValue && !_withLiteral) [ret appendString:@"-"];
        
		[ret appendString:[_degreesFormatter stringForObjectValue:[NSNumber numberWithDouble:degrees]]];
		[ret appendString:_descDegrees];
		
		if (_withMinutes) {
			[ret appendString:[_minutesFormatter stringForObjectValue:[NSNumber numberWithDouble:minutes]]];
			[ret appendString:_descMinutes];		
		}
		
		if (_withSeconds) {
			[ret appendString:[_secondsFormatter stringForObjectValue:[NSNumber numberWithDouble:seconds]]];
			[ret appendString:_descSeconds];
		}
		
		if (_withLiteral) {
			if (_longitude) {						
				if (negativeValue) [ret appendString:@"W"];
				else [ret appendString:@"E"];
			} else {
				if (negativeValue) [ret appendString:@"S"];
				else [ret appendString:@"N"];
			}
		}
		
		return ret;
	}
	
	
	return @"";
}

- (BOOL)getObjectValue:(id *)anObject forString:(NSString *)string errorDescription:(NSString **)error 
{
	NSScanner * scanner = [NSScanner scannerWithString:string];
	if (_localized) [scanner setLocale:_myLocale];
    
	double scanned = 0; 
	double value = 0;
	BOOL degreesFound = NO;
	BOOL minutesFound = NO;
	BOOL secondsFound = NO;
	BOOL valueFound = NO;
	BOOL negativeValue = NO;
	
	NSArray * array = [_descDegrees componentsSeparatedByString:@" "];
	NSString * degreesString = [array objectAtIndex:0];
	array = [_descMinutes componentsSeparatedByString:@" "];
	NSString * minutesString = [array objectAtIndex:0];
	array = [_descSeconds componentsSeparatedByString:@" "];
	NSString * secondsString = [array objectAtIndex:0];
	
	while ([scanner scanDouble:&scanned]) {
		if ([scanner scanString:degreesString intoString:nil]) {
			// the scanned value is degrees
			value += scanned;
			if (value < .0) {
				value = -value;
				negativeValue = YES;
			}
			if (degreesFound) {
				if (error != NULL)
					*error = @"A degrees value should appear once";
				return NO;
			}
			if (secondsFound || minutesFound) {
				if (error != NULL)
					*error = @"The degrees should appear before the seconds and minutes";
				return NO;
			}
			degreesFound = YES;
			valueFound = YES;
		} else if ([scanner scanString:minutesString intoString:nil]) {
			// the scanned value is minutes
			if (scanned >= 60. || scanned <= -60.) {
				if (error != NULL)
					*error = @"Minutes out of bounds (should be <60)";
				return NO;
			}
			if (valueFound && scanned < 0.) {
				if (error != NULL)
					*error = @"Minutes out of bounds (should be >0)";
				return NO;
			} 
			if (value < .0) {
				value = -value;
				negativeValue = YES;
			}
			if (minutesFound){
				if (error != NULL)
					*error = @"A minutes value should appear once";
				return NO;
			}
			if (secondsFound) {
				if (error != NULL)
					*error = @"The minutes should appear before the seconds";
				return NO;
			}
			value += scanned / 60.;
			minutesFound = YES;
			valueFound = YES;
            
		} else if ([scanner scanString:secondsString intoString:nil]) {
			// the scanned value is seconds
			if (scanned >= 60. || scanned <= -60.) {
				if (error != NULL)
					*error = @"Seconds out of bounds (should be <60)";
				return NO;
			}
            
			if (valueFound && scanned < 0.) {
				if (error != NULL)
					*error = @"Seconds out of bounds (should be >0)";
				return NO;
			} 
			if (value < .0) {
				value = -value;
				negativeValue = YES;
			}
			if (secondsFound){
				if (error != NULL)
					*error = @"A minutes value should appear once";
				return NO;
			}
			value += scanned / 3600.;
			secondsFound = YES;
			valueFound = YES;
		} else {
			// First we check negative value integrity : only one (and tyhe first) negative value
			if (scanned < .0) {
				if (valueFound) {
					if (error != NULL) 
						*error = @"Only the first value should be negative";
					return NO;
				}
				scanned = -scanned;
				negativeValue = YES;
			}
			
			// We don't know what the value is, trying to guess it
			if (!degreesFound) {
				value += scanned;
				degreesFound = YES;
			} else if (!minutesFound) {
				if (scanned >= 60. || scanned <= -60.) {
					if (error != NULL)
						*error = @"Minutes out of bounds (should be <60)";
					return NO;
				}
				if (valueFound && scanned < 0.) {
					if (error != NULL)
						*error = @"Minutes out of bounds (should be >0)";
					return NO;
				} 
				value += scanned / 60.;
				minutesFound = YES;
			} else if (!secondsFound) {
				if (scanned >= 60. || scanned <= -60.) {
					if (error != NULL)
						*error = @"Seconds out of bounds (should be <60)";
					return NO;
				}
				if (valueFound && scanned < 0.) {
					if (error != NULL)
						*error = @"Seconds out of bounds (should be >0)";
					return NO;
				}
				value += scanned / 3600.;
				secondsFound = YES;
			} else {			
				if (error != NULL) 
					*error = @"Too much values founds";
				return NO;
			}
			valueFound = YES;
		}
	}	
	
	NSCharacterSet * set = [NSCharacterSet characterSetWithCharactersInString:@"NSEW"];
	NSString * cardValue;
	
	if ([scanner scanCharactersFromSet:set intoString:&cardValue]) {
		if (negativeValue) {
			if (error != NULL)
				*error = @"Cannot manage negative entry with a cardinal value";
			return NO;
		}
		
		if (_longitude) {
			if ([cardValue isEqualToString:@"W"]){
				value = -value;
			} else if ([cardValue isEqualToString:@"S"] || [cardValue isEqualToString:@"N"]) {
				if (error != NULL)
					*error = @"Only E or W are allowed for a longitude";
				return NO;
			}
		} else {
			if ([cardValue isEqualToString:@"S"]){
				value = -value;
			} else if ([cardValue isEqualToString:@"W"] || [cardValue isEqualToString:@"E"]) {
				if (error != NULL)				
					*error = @"Only S or N are allowed for a latitude";
				return NO;
			}
		}
	} else {
		if (! [scanner isAtEnd]) {
			if (error != NULL)
				*error = @"Invalid character found";
			return NO;
		}
	}
	// bounds tests
	if (_longitude && (value > 180.0) ) {
		if (error != NULL)
			*error = @"Value out of bounds ( >180 or <-180 )";
		return NO;
	}
	if (!_longitude && (value > 90.0)) {
		if (error != NULL)
			*error = @"Value out of bounds ( >90 or <-90 )";
		return NO;
	}
	
	if (negativeValue) value = -value;
	*anObject = [NSNumber numberWithDouble:value];
	return YES;
    
}


- (NSAttributedString *)attributedStringForObjectValue:(id)anObject withDefaultAttributes:(NSDictionary *)attributes {
	return nil;
}

- (void)dealloc
{
	[_degreesFormatter release];
    [_minutesFormatter release];
	[_secondsFormatter release];
	[super dealloc];
}

#pragma mark -
#pragma mark NSCopying protocol

- (id)copyWithZone:(NSZone *)zone {
    __MY_NSLog(@"%s", __FUNCTION__  )
    JMLongitudeLatitudeFormatter *copyObj = [super copyWithZone:zone];
    if (copyObj) {
        [copyObj setWithMinutes:_withMinutes];
        [copyObj setWithSeconds:_withSeconds];
        [copyObj setWithLiteral:_withLiteral];
        [copyObj setLongitude:_longitude];
        [copyObj setLocalized:_localized];
        [copyObj setDegreesFormatter:_degreesFormatter];
        [copyObj setMinutesFormatter:_minutesFormatter];
        [copyObj setSecondsFormatter:_secondsFormatter];
        [copyObj setDescDegrees:_descDegrees];
        [copyObj setDescMinutes:_descMinutes];
        [copyObj setDescSeconds:_descSeconds];
        [copyObj setMyLocale:_myLocale];
    }
    return copyObj;
}

#pragma mark -
#pragma mark NSCoding protocol

- (void)encodeWithCoder:(NSCoder *)coder {
    [super encodeWithCoder:coder];
    [coder encodeBool:[self withMinutes] forKey:@"_withMinutes"];
    [coder encodeBool:[self withSeconds] forKey:@"_withSeconds"];
    [coder encodeBool:[self withLiteral] forKey:@"_withLiteral"];
    [coder encodeBool:[self longitude] forKey:@"_longitude"];
    [coder encodeBool:[self localized] forKey:@"_localized"];
    [coder encodeObject:[self degreesFormatter] forKey:@"_degreesFormatter"];
    [coder encodeObject:[self minutesFormatter] forKey:@"_minutesFormatter"];
    [coder encodeObject:[self secondsFormatter] forKey:@"_secondsFormatter"];
    [coder encodeObject:[self descDegrees] forKey:@"_descDegrees"];
    [coder encodeObject:[self descMinutes] forKey:@"_descMinutes"];
    [coder encodeObject:[self descSeconds] forKey:@"_descSeconds"];
    [coder encodeObject:[self myLocale] forKey:@"_myLocale"];
}

- (id)initWithCoder:(NSCoder *)coder {
    if ([super initWithCoder:coder]) {
        [self setWithMinutes:[coder decodeBoolForKey:@"_withMinutes"]];
        [self setWithSeconds:[coder decodeBoolForKey:@"_withSeconds"]];
        [self setWithLiteral:[coder decodeBoolForKey:@"_withLiteral"]];
        [self setLongitude:[coder decodeBoolForKey:@"_longitude"]];
        [self setLocalized:[coder decodeBoolForKey:@"_localized"]];
        [self setDegreesFormatter:[coder decodeObjectForKey:@"_degreesFormatter"]];
        [self setMinutesFormatter:[coder decodeObjectForKey:@"_minutesFormatter"]];
        [self setSecondsFormatter:[coder decodeObjectForKey:@"_secondsFormatter"]];
        [self setDescDegrees:[coder decodeObjectForKey:@"_descDegrees"]];
        [self setDescMinutes:[coder decodeObjectForKey:@"_descMinutes"]];
        [self setDescSeconds:[coder decodeObjectForKey:@"_descSeconds"]];
        [self setMyLocale:[coder decodeObjectForKey:@"_myLocale"]];
    }
    return self;
}

#pragma mark -
#pragma mark Accessor (respect KVC-KVO syntax)

//---------------------------------------------------------- 
//  withMinutes 
//---------------------------------------------------------- 
- (BOOL)withMinutes { return _withMinutes; }
- (void)setWithMinutes:(BOOL)flag {
    _withMinutes = flag;
    if (!flag) [self setWithSeconds:flag];
}

//---------------------------------------------------------- 
//  withSeconds 
//---------------------------------------------------------- 
- (BOOL)withSeconds { return _withSeconds; }
- (void)setWithSeconds:(BOOL)flag {
    _withSeconds = flag;
    if (flag) [self setWithMinutes:flag];
}

//---------------------------------------------------------- 
//  withLiteral 
//---------------------------------------------------------- 
- (BOOL)withLiteral { return _withLiteral; }
- (void)setWithLiteral:(BOOL)flag {
    _withLiteral = flag;
}

//---------------------------------------------------------- 
//  longitude 
//---------------------------------------------------------- 
- (BOOL)longitude { return _longitude; }
- (void)setLongitude:(BOOL)flag {
    _longitude = flag;
}

//---------------------------------------------------------- 
//  localized 
//---------------------------------------------------------- 
- (BOOL)localized { return _localized; }
- (void)setLocalized:(BOOL)flag {
    _localized = flag;
}

//---------------------------------------------------------- 
//  degreesFormatter 
//---------------------------------------------------------- 
- (NSNumberFormatter *)degreesFormatter { return [[_degreesFormatter retain] autorelease]; }
- (void)setDegreesFormatter:(NSNumberFormatter *)aDegreesFormatter {
    if (_degreesFormatter != aDegreesFormatter) {
        [_degreesFormatter release];
        _degreesFormatter = [aDegreesFormatter copy];
    }
}

//---------------------------------------------------------- 
//  minutesFormatter 
//---------------------------------------------------------- 
- (NSNumberFormatter *)minutesFormatter { return [[_minutesFormatter retain] autorelease]; }
- (void)setMinutesFormatter:(NSNumberFormatter *)aMinutesFormatter {
    if (_minutesFormatter != aMinutesFormatter) {
        [_minutesFormatter release];
        _minutesFormatter = [aMinutesFormatter copy];
    }
}

//---------------------------------------------------------- 
//  secondsFormatter 
//---------------------------------------------------------- 
- (NSNumberFormatter *)secondsFormatter { return [[_secondsFormatter retain] autorelease]; }
- (void)setSecondsFormatter:(NSNumberFormatter *)aSecondsFormatter {
    if (_secondsFormatter != aSecondsFormatter) {
        [_secondsFormatter release];
        _secondsFormatter = [aSecondsFormatter copy];
    }
}

//---------------------------------------------------------- 
//  descDegrees 
//---------------------------------------------------------- 
- (NSString *)descDegrees { return [[_descDegrees retain] autorelease]; }
- (void)setDescDegrees:(NSString *)aDescDegrees {
    if (_descDegrees != aDescDegrees) {
        [_descDegrees release];
        _descDegrees = [aDescDegrees copy];
    }
}

//---------------------------------------------------------- 
//  descMinutes 
//---------------------------------------------------------- 
- (NSString *)descMinutes { return [[_descMinutes retain] autorelease]; }
- (void)setDescMinutes:(NSString *)aDescMinutes {
    if (_descMinutes != aDescMinutes) {
        [_descMinutes release];
        _descMinutes = [aDescMinutes copy];
    }
}

//---------------------------------------------------------- 
//  descSeconds 
//---------------------------------------------------------- 
- (NSString *)descSeconds { return [[_descSeconds retain] autorelease]; }
- (void)setDescSeconds:(NSString *)aDescSeconds {
    if (_descSeconds != aDescSeconds) {
        [_descSeconds release];
        _descSeconds = [aDescSeconds copy];
    }
}

//---------------------------------------------------------- 
//  myLocale 
//---------------------------------------------------------- 
- (NSDictionary *)myLocale { return [[_myLocale retain] autorelease]; }
- (void)setMyLocale:(NSDictionary *)aMyLocale {
    if (_myLocale != aMyLocale) {
        [_myLocale release];
        _myLocale = [aMyLocale copy];
    }
}



@end
