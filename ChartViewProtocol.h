/*
 *  ChartViewProtocol.h
 *  RasterChart2BSB
 *
 *  Created by Nicolas Cherel on 13/10/05.
 *  Copyright 2005 Magic Instinct Software.
 *
 */

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#import <Cocoa/Cocoa.h>

@class ChartDocumentController;

@protocol ChartView

- (id)initWithController:(ChartDocumentController *)controller;
- (void)setUsesTransparency:(BOOL)b forPoint:(NSPoint)p;
- (void)translatePoint:(NSPoint)p withDx:(float)x withdy:(float)y;
- (void)setImageZoom:(double)scale;
- (void)translatePoint:(NSPoint)p withDx:(float)x withdy:(float)y;
- (void)deSelectPoint;
@end
