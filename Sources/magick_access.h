/*
 *  magick_access.h
 *  MagickTest
 *
 *  Created by Nicolas Cherel on 16/06/05.
 *  Copyright 2005 Magic Instinct Software.
 *
 */
 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 
enum {
	MAGICK_QUANTIZE_ERROR,
	MAGICK_CANT_SET_RESSOURCE_LIMIT,
	MAGICK_CANT_LOAD_FILE,
	MAGICK_CANT_ALLOCATE_MEM,
	MAGICK_CANT_MAKE_AN_INDEXED_IMAGE
};

typedef int (* monitor_type) (const char *text,  long long offset, unsigned long long span, void * clientData);

char * getMagickErrorString(int error);

int convertFileIntoTIFF128Colors (const char * inputFilePath, monitor_type monitor, void * monitor_user_data, int dither, unsigned char ** data, size_t * length);

int monitorReturnYes(void);
int monitorReturnNo(void);
