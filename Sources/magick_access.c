/*
 *  magick_access.c
 *  MagickTest
 *
 *  Created by Nicolas Cherel on 16/06/05.
 *  Copyright 2005 Magic Instinct Software. All rights reserved.
 *
 */
 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <wand/magick_wand.h>
#include "magick_access.h"

int monitorReturnYes(void) {
	return MagickTrue;
}

int monitorReturnNo(void) {
	return MagickFalse;
}


char * getMagickErrorString(int error) {
	switch(error) {
	case MAGICK_QUANTIZE_ERROR : {
		return "Error while image quantize (image color reducing)";
	}
	case MAGICK_CANT_SET_RESSOURCE_LIMIT : {
		return "Can't limit ressource";
	}
	case MAGICK_CANT_LOAD_FILE : {
		return "Temporary file cannot be loaded";
	}
	case MAGICK_CANT_ALLOCATE_MEM : {
		return "Memory cannot be allocted";
	}
	}
	return "Unknown";
}


int convertFileIntoTIFF128Colors (const char * inputFilePath, monitor_type monitor, void * monitor_user_data, int dither, unsigned char ** data, size_t * length)
{
	MagickWand * magick_wand = NewMagickWand();
	
	if (magick_wand == NULL)
		return MAGICK_CANT_ALLOCATE_MEM;
	
	MagickSetProgressMonitor(magick_wand,(MagickProgressMonitor) monitor, monitor_user_data);
    MagickBooleanType status = MagickReadImage(magick_wand, inputFilePath);
	if (status == MagickFalse)
		return MAGICK_CANT_LOAD_FILE;
	  
	SetImageProgressMonitor(GetImageFromMagickWand(magick_wand), (MagickProgressMonitor) monitor, monitor_user_data);

	MagickSetResourceLimit(MemoryResource, 80);
	if (status == MagickFalse)
		return MAGICK_CANT_SET_RESSOURCE_LIMIT;
		
	status = MagickQuantizeImage(magick_wand,127,RGBColorspace,0,dither,0);
	if (status == MagickFalse)
		return MAGICK_QUANTIZE_ERROR;
	
	if (MagickGetImageType(magick_wand) != PaletteType) {
		return MAGICK_CANT_MAKE_AN_INDEXED_IMAGE;
	}
	
	MagickSetImageCompression(magick_wand, NoCompression);
	MagickSetImageFormat(magick_wand, "TIFF");
	
	*data = MagickGetImageBlob(magick_wand, length);
	
	magick_wand=DestroyMagickWand(magick_wand);
	return 0;
}