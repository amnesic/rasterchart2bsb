/* ChartViewCG */

//  Created by Nicolas Cherel.
//  Copyright 2005 Magic Instinct Software. 
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>
#import "ChartDocumentController.h"

#import "ChartViewProtocol.h"

@interface ChartViewCG : NSView <ChartView, ChartViewCGProtocol>
{
	id 					_controller;
	NSPoint				_lensCenter;
	NSPoint     		_beforeDragPos;
	NSMutableArray		*_plotedImages;
	NSDictionary		*_buttons;
	CIImage				*_image;
	CIImage				*_imageOrig;
	CIImage				*_flag_select;
	CIImage				*_flag_selected;
	CIFilter			*_lens;
	CIFilter			*_compose;
	CIFilter			*_scale;
	CGSize				_lensMouseDragDelta;

// updated in viewScrolled: needed because sometimes _visibleRect returns
// garbage
	NSRect				_visibleRect;
	
// Lens motion control
	BOOL				_lensMoveLaunched;
	BOOL				_lensMoveCancel;
	NSPoint				_lensDestination;
	
	double				_imageScale;
	double				_magnifyScale;
	float 				_marginsWidth;
	BOOL				_drag;
}

- (id)initWithController:(ChartDocumentController *)controller;
- (void)setImageData:(NSData *)data andMarginsWidth:(float)width;
- (NSSize)imageSize;
- (void)setImageZoom:(double)scale;
- (void)setLensMagnification:(double)scale;
- (void)initViewFrame;
- (void)selectPoint:(NSPoint)p;

- (void)setUsesTransparency:(BOOL)b forPoint:(NSPoint)p;
- (void)translatePoint:(NSPoint)p withDx:(float)x withdy:(float)y;

- (id)buttonUnderPoint:(NSPoint)p;

-(void)bringLensToPoint:(NSPoint)p;

- (void)performButtonAdd;
- (void)performButtonRecenter;
- (void)performButtonDelete;

@end
