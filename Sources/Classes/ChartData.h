//
//  ChartData.h
//  RasterChart2BSB
//
//  Created by Nicolas Cherel on 08/07/05.
//  Copyright 2005 Magic Instinct Software.  
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

//

#import <Cocoa/Cocoa.h>

@class ChartData;

@protocol ChartDataExporter
- (id)initWithObserver:(id)anObserver andSelector:(SEL)aSelector;
- (NSData *)exportReferencedImage:(NSData *)anImage withGeoData:(ChartData *)someData;
@end

@interface ChartData : NSObject {
	NSMutableDictionary		*_dictForData;
}

#pragma mark init
- (ChartData *)initWithDictionary:(NSDictionary *)dict;

#pragma mark DataSources accessors
- (id)refPointsTableDataSource;
- (id)plyPointsTableDataSource;

#pragma mark Setters
- (void)setBSBVersion:(NSString *)aVer;
- (void)setImageWidth:(NSNumber *)aWidth;
- (void)setImageHeight:(NSNumber *)aHeight;
- (void)setChartName:(NSString *)aName;
- (void)setDPI:(NSNumber *)aDPI;
- (void)setEditDate:(NSDate *)aDate;
- (void)setGeodeticDatum:(NSString *)aDatum;
- (void)setProjection:(NSString *)aProjectionName;
- (void)setProjectionParameter:(NSNumber *)aProjectionParameter;
- (void)setScale:(NSNumber *)aScale;
- (void)setSkewAngle:(NSNumber *)aSkewAngle;
- (void)setSoundingDatum:(NSString *)aSoundingDatum;
- (void)setUnit:(NSString *)aUnit;

#pragma mark Getters
- (NSString *)BSBVersion;
- (NSNumber *)imageWidth;
- (NSNumber *)imageHeight;
- (NSString *)chartName;
- (NSNumber *)DPI;
- (NSCalendarDate *)editDate;
- (NSString *)geodeticDatum;
- (NSString *)projection;
- (NSNumber *)projectionParameter;
- (NSNumber *)scale;
- (NSNumber *)skewAngle;
- (NSString *)soundingDatum;
- (NSString *)unit;
- (NSDictionary *)refPoints;
- (NSDictionary *)plyPoints;

// Guess ply points order from ref points coords
- (NSArray *)autoSortedPlyPoints;
// Make the widest polygone and guess correspondind lat lon
- (NSArray *)autoMaxAreaPlyPoints;


@end


