//
//  ImageViewWarpper.h
//  RasterChart2BSB
//
//  Created by Nicolas Cherel on 05/07/05.
//  Copyright 2005 Magic Instinct Software.  
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//

#import <Cocoa/Cocoa.h>


@interface ImageViewWarpper : NSView {
	NSImageView 		*_imageView;
	float 				_marginsWidth;
	
// NSRect backup to test and avoid setFrame loop
	NSRect				_lastSetFrame;	
	NSRect				_lastSetViewFrame;
}

- (ImageViewWarpper *) initWithImageView:(NSImageView *)imageView andMarginsWidth:(float)width;
- (NSSize)imageSize;
- (void)setImageZoom:(double)scale;
- (void)initViewFrame;
@end
