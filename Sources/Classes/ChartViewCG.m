
//  Created by Nicolas Cherel.
//  Copyright 2005 Magic Instinct Software. 
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#import "ChartViewCG.h"
#import "Button.h"

static CIImage * BUTTON_PLUS_UP;
static CIImage * BUTTON_PLUS;
static CIImage * BUTTON_PLUS_DOWN;
static CIImage * BUTTON_PLUS_DISABLED;

static CIImage * BUTTON_RECENTER_UP;
static CIImage * BUTTON_RECENTER;
static CIImage * BUTTON_RECENTER_DOWN;
static CIImage * BUTTON_RECENTER_DISABLED;

static CIImage * BUTTON_REMOVE_UP;
static CIImage * BUTTON_REMOVE;
static CIImage * BUTTON_REMOVE_DOWN;
static CIImage * BUTTON_REMOVE_DISABLED;

static CIImage * FLAG;
static CIImage * FLAG_TRANSPARENT;
static NSPoint FLAG_CENTER;
static CIImage * FLAG_SELECT;
static NSPoint FLAG_SELECT_CENTER;

#define max(X,Y) ((X) > (Y) ? (X) : (Y))
#define min(X,Y) ((X) < (Y) ? (X) : (Y))

// Correction routines
static inline BOOL isPointInCircle(NSPoint p, NSPoint center, float radius) {
	NSPoint distFromCenter;
	distFromCenter.x = (center.x - p.x);
	distFromCenter.y = (center.y - p.y);
	return sqrtf(powf(distFromCenter.x, 2) + powf(distFromCenter.y, 2)) < radius;
}

static inline BOOL isPointInRect(NSPoint p, NSRect r) {
	return (p.x > r.origin.x 
		&& p.x - r.origin.x < r.size.width 	
		&& p.y > r.origin.y
		&& p.y - r.origin.y < r.size.height);
}	

// Check if the point is in a rectangle of a center (diag cross position) o and a s heigth and width
static inline BOOL isPointInArea(NSPoint p, NSPoint o, NSSize s){
	return fabsf (p.x - o.x) < s.width / 2 && fabsf (p.y - o.y) < s.height / 2;
}

static inline NSPoint scalePointWithFactor(NSPoint p, NSPoint center, double rescaleFactor) {
	NSPoint distFromCenter;
	distFromCenter.x = (center.x - p.x);
	distFromCenter.y = (center.y - p.y);
	NSPoint ret;
	ret.x = center.x - ( distFromCenter.x * rescaleFactor);
	ret.y = center.y - ( distFromCenter.y * rescaleFactor);
	return ret;
}

// CoreImage/Cocoa data convertion

static inline NSRect makeNSRectFromCGRect(CGRect r) {
	return *((NSRect *)(&r));
}
static inline NSSize makeNSSizeFromCGSize(CGSize r) {
	return *((NSSize *)(&r));
}
static inline NSPoint makeNSPointFromCGPoint(CGPoint r) {
	return *((NSPoint *)(&r));
}

@implementation ChartViewCG

+ (void)initialize
{
// init static variables, common to all instances
	BUTTON_PLUS = [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"button-plus-normal" ofType:@"png"]]];
	[BUTTON_PLUS retain];
	BUTTON_PLUS_UP = [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"button-plus-up" ofType:@"png"]]];
	[BUTTON_PLUS_UP retain];
	BUTTON_PLUS_DOWN = [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"button-plus-down" ofType:@"png"]]];
	[BUTTON_PLUS_DOWN retain];
	BUTTON_PLUS_DISABLED = [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"button-plus-disabled" ofType:@"png"]]];
	[BUTTON_PLUS_DISABLED retain];
	BUTTON_RECENTER = [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"button-recenter-normal" ofType:@"png"]]];
	[BUTTON_RECENTER retain];
	BUTTON_RECENTER_UP = [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"button-recenter-up" ofType:@"png"]]];
	[BUTTON_RECENTER_UP retain];
	BUTTON_RECENTER_DOWN = [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"button-recenter-down" ofType:@"png"]]];
	[BUTTON_RECENTER_DOWN retain];
	BUTTON_RECENTER_DISABLED = [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"button-recenter-disabled" ofType:@"png"]]];
	[BUTTON_RECENTER_DISABLED retain];
	BUTTON_REMOVE = [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"button-remove-normal" ofType:@"png"]]];
	[BUTTON_REMOVE retain];
	BUTTON_REMOVE_UP = [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"button-remove-up" ofType:@"png"]]];
	[BUTTON_REMOVE_UP retain];
	BUTTON_REMOVE_DOWN = [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"button-remove-down" ofType:@"png"]]];
	[BUTTON_REMOVE_DOWN retain];
	BUTTON_REMOVE_DISABLED = [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"button-remove-disabled" ofType:@"png"]]];
	[BUTTON_REMOVE_DISABLED retain];
	FLAG = [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"flag" ofType:@"png"]]];
	[FLAG retain];
	FLAG_TRANSPARENT = [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"flag-transparent" ofType:@"png"]]];
	[FLAG_TRANSPARENT retain];
	NSScanner * scanFlagCenter = [NSScanner scannerWithString:[NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"flag-center" ofType:@"txt"]]];
	[scanFlagCenter scanFloat:&FLAG_CENTER.x];
	[scanFlagCenter scanString:@"," intoString:nil];
	[scanFlagCenter scanFloat:&FLAG_CENTER.y];
	FLAG_CENTER.y = fabsf(FLAG_CENTER.y - [FLAG extent].size.height);
	FLAG_SELECT = [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"flag-select" ofType:@"png"]]];
	[FLAG_SELECT retain];
	NSScanner * scanFlagGlowCenter = [NSScanner scannerWithString:[NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"flag-select-center" ofType:@"txt"]]];
	[scanFlagGlowCenter scanFloat:&FLAG_SELECT_CENTER.x];
	[scanFlagGlowCenter scanString:@"," intoString:nil];
	[scanFlagGlowCenter scanFloat:&FLAG_SELECT_CENTER.y];
	FLAG_SELECT_CENTER.y = fabsf(FLAG_SELECT_CENTER.y - [FLAG_SELECT extent].size.height);
}


- (id)initWithController:(ChartDocumentController *)controller
{
	[super init];
	_controller = controller;
	_imageScale = .5;
	_magnifyScale = 2.;	
	_plotedImages = [[NSMutableArray alloc] init];
	
	
	NSArray * filterList = [CIFilter filterNamesInCategories:nil];
	if(![filterList containsObject:@"LensFilter"])
    {
		NSString    *path = [[[NSBundle mainBundle] builtInPlugInsPath] stringByAppendingPathComponent:@"LensImageUnit.plugin"];
		NSURL	    *pluginURL = [NSURL fileURLWithPath:path];
		[CIPlugIn loadPlugIn:pluginURL allowNonExecutable:NO];
	}
	
	filterList = [CIFilter filterNamesInCategories:nil];
	if (![filterList containsObject:@"LensFilter"]){
		NSRunCriticalAlertPanel(@"Required filters are missing", @"Can't find Lens Image Unit", @"Quit", nil, nil, nil);
		[NSApp terminate:self];
	} 
		
	_lens = [CIFilter filterWithName:@"LensFilter"];
	[_lens retain];
	[_lens setValue:[CIVector vectorWithX:_lensCenter.x Y:_lensCenter.y] forKey:@"inputCenter"];
	[_lens setValue:[NSNumber numberWithDouble:_magnifyScale] forKey:@"inputMagnification"];
	[_lens setValue:[NSNumber numberWithDouble:0.86] forKey:@"inputRoundness"];
	[_lens setValue:[NSNumber numberWithDouble:0.90] forKey:@"inputShineOpacity"];
	[_lens setValue:[NSNumber numberWithDouble:9.2] forKey:@"inputRingFilletRadius"];
	[_lens setValue:[NSNumber numberWithDouble:22] forKey:@"inputRingWidth"];
	[_lens setValue:[NSNumber numberWithDouble:250.00] forKey:@"inputWidth"];

// init buttons with the "normal", "up", "down" and "disable" states
	_buttons = [[NSMutableDictionary dictionaryWithObjectsAndKeys:
			[[Button buttonWithImages:[NSDictionary dictionaryWithObjectsAndKeys:
						BUTTON_PLUS,@"normal",
						BUTTON_PLUS_UP,@"up",
						BUTTON_PLUS_DOWN,@"down",
						BUTTON_PLUS_DISABLED,@"disabled",nil]
					withAction:@selector(performButtonAdd)
					onObject:self] retain],
			@"plus",
			[[Button buttonWithImages:[NSDictionary dictionaryWithObjectsAndKeys:
						BUTTON_REMOVE,@"normal",
						BUTTON_REMOVE_UP,@"up",
						BUTTON_REMOVE_DOWN,@"down",
						BUTTON_REMOVE_DISABLED,@"disabled",nil]
					withAction:@selector(performButtonDelete)
					onObject:self] retain],
			@"remove",
			[[Button buttonWithImages:[NSDictionary dictionaryWithObjectsAndKeys:
						BUTTON_RECENTER,@"normal",
						BUTTON_RECENTER_UP,@"up",
						BUTTON_RECENTER_DOWN,@"down",
						BUTTON_RECENTER_DISABLED,@"disabled",nil]
					withAction:@selector(performButtonRecenter)
					onObject:self] retain],
			@"recenter",nil]
		 retain];
	[[_buttons objectForKey:@"plus"] setCurrentImage:@"normal"];
	[[_buttons objectForKey:@"recenter"] setCurrentImage:@"disabled"];
	[[_buttons objectForKey:@"remove"] setCurrentImage:@"disabled"];
	
	_compose = [CIFilter filterWithName:@"CISourceOverCompositing"];
	[_compose retain];
	
	_scale = [CIFilter filterWithName:@"CIAffineTransform"];
    NSAffineTransform *transform = [NSAffineTransform transform];
    [transform scaleBy:_imageScale];
    [_scale setValue:transform forKey:@"inputTransform"];
	[_scale retain];

	return self;
}

- (void)viewDidMoveToSuperview
{
	if ([[self superview] isKindOfClass:[NSClipView class]]) {
		[[self superview] setPostsBoundsChangedNotifications:YES];
		[[NSNotificationCenter defaultCenter] addObserver:self 
			selector:@selector(viewScrolled:) name:NSViewBoundsDidChangeNotification object:[self superview]];
	} 
}

-(void)setImageData:(NSData *)data andMarginsWidth:(float)width
{
	float minMarginSize = [[_lens valueForKey:@"inputWidth"] floatValue] / 2 + [[_lens valueForKey:@"inputRingWidth"] floatValue] + [BUTTON_PLUS extent].size.width;
	_marginsWidth = width > minMarginSize ? width : minMarginSize;
	_lensCenter.x = _marginsWidth;
	_lensCenter.y = _marginsWidth;
	
	if (_imageOrig != nil) {
		[_imageOrig release];
		 _imageOrig = nil; 
	}
	if (_image != nil) {
		[_image release];
		_image = nil;
	}
	
	NSBitmapImageRep * img = [NSBitmapImageRep imageRepWithData:data];
	NSDictionary * decompOptions = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:NSTIFFCompressionNone] forKey:NSImageCompressionMethod];
	NSData * imgDecompData = [[img representationUsingType:NSTIFFFileType properties:decompOptions] retain];
	
	NSDictionary * theOptions = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:kCIFormatARGB8],kCGColorSpaceGenericRGB,nil]
			forKeys:[NSArray arrayWithObjects:@"CIFormat",kCIImageColorSpace,nil]];
			
	_imageOrig = [[CIImage imageWithData:imgDecompData options:theOptions] retain];
	CIFilter *scale = [CIFilter filterWithName:@"CIAffineTransform"];
    NSAffineTransform *transform = [NSAffineTransform transform];
    [transform scaleBy:_imageScale];
    [scale setValue:transform forKey:@"inputTransform"];
	
	[scale setValue:_imageOrig forKey:@"inputImage"];
	_image = [scale valueForKey:@"outputImage"];
	_image = [_image imageByApplyingTransform:CGAffineTransformMakeTranslation (_marginsWidth, _marginsWidth)];
	[_image retain];
	
	_lensCenter.y = [_image extent].size.height + _marginsWidth;
	
	[self initViewFrame];
}

- (void)initViewFrame
{
	
	NSRect newFrame;
	CGRect imgRect = [_imageOrig extent];
	newFrame = *((NSRect *)&imgRect);
	
	newFrame.size.width *= _imageScale;
	newFrame.size.height *= _imageScale;
	newFrame.origin.x += _marginsWidth;
	newFrame.origin.y += _marginsWidth;
	newFrame.size.width += _marginsWidth * 2;
	newFrame.size.height += _marginsWidth * 2;
	[self setFrame:newFrame];
}

- (NSSize)imageSize
{
	CGSize tmp = [_imageOrig extent].size;
	return *((NSSize *) &tmp);
}

- (void)viewScrolled:(NSNotification *)aNotification
{
	_visibleRect = [self visibleRect];
	[self setNeedsDisplay:YES];
}

- (CIImage *)placeImage:(CIImage *)image
{
	CGRect newFrame = [image extent];
	NSRect imgRect = _visibleRect;
	
	float decayX = _marginsWidth, decayY = _marginsWidth;
	
	newFrame.size.width += _marginsWidth * 2;
	if (newFrame.size.width < imgRect.size.width) {
		decayX = (imgRect.size.width - newFrame.size.width) / 2;
		newFrame.size.width = imgRect.size.width;
	}
	
	newFrame.size.height += _marginsWidth * 2;
	if (newFrame.size.height < imgRect.size.height) {
		decayY = (imgRect.size.height - newFrame.size.height) / 2;
		newFrame.size.height = imgRect.size.height;
	}
	
	if (newFrame.origin.x != decayX || newFrame.origin.y != decayY) {
		image = [image imageByApplyingTransform:CGAffineTransformMakeTranslation (decayX,decayY)];
	}

	return image;
}

- (void)setLensMagnification:(double)scale
{
	_magnifyScale = scale / _imageScale;
	[_lens setValue:[NSNumber numberWithDouble:_magnifyScale] forKey:@"inputMagnification"];
}
 
- (void)setImageZoom:(double)scale
{
	double oldScale = _imageScale;
	_imageScale = scale;
	
	// Magnify scale update (to keep same lens appearence)
	_magnifyScale = _magnifyScale / ( scale / oldScale);
	[_lens setValue:[NSNumber numberWithDouble:_magnifyScale] forKey:@"inputMagnification"];
	
    NSAffineTransform *transform = [NSAffineTransform transform];
    [transform scaleBy:_imageScale];
    [_scale setValue:transform forKey:@"inputTransform"];

	NSPoint newLensCenter;
	newLensCenter.x = ((_lensCenter.x - _marginsWidth) * (scale / oldScale)) + _marginsWidth;
	newLensCenter.y = ((_lensCenter.y - _marginsWidth) * (scale / oldScale)) + _marginsWidth;
	_lensCenter = newLensCenter;

	[self initViewFrame];
}


#pragma mark -
#pragma mark Plot & Points managment

- (NSPoint)convertPointForChart:(NSPoint)locationInView
{
	NSPoint newPoint = locationInView;
	newPoint.x -= _marginsWidth;
	newPoint.y -= _marginsWidth;
	newPoint.x /= _imageScale;
	newPoint.x = roundf (newPoint.x);
	newPoint.y /= _imageScale;
	newPoint.y = roundf (fabsf (newPoint.y - [_imageOrig extent].size.height));
	return newPoint;
}

- (NSPoint)convertPointForZoomedView:(NSPoint)locationInChart
{
	NSPoint newPoint = locationInChart;
	newPoint.y = - (newPoint.y - [_imageOrig extent].size.height );
	newPoint.x *= _imageScale;
	newPoint.y *= _imageScale;
	newPoint.x += _marginsWidth;
	newPoint.y += _marginsWidth;
	return newPoint;
}

// Return new location in Chart
- (NSPoint)plotPoint:(NSPoint)locationInView
{
	[_plotedImages addObject:[FLAG imageByApplyingTransform:CGAffineTransformMakeTranslation((locationInView.x - _marginsWidth )/_imageScale - FLAG_CENTER.x , (locationInView.y - _marginsWidth )/_imageScale - FLAG_CENTER.y)]];
	return [self convertPointForChart:locationInView];
}

- (id)findPoint:(NSPoint)locationInChart
{
	unsigned i = 0;
	unsigned nbObjs = [_plotedImages count];
	CIImage * obj;
	while (i < nbObjs){
		obj = [_plotedImages objectAtIndex:i];
		CGPoint objPos = [obj extent].origin;
		objPos.x = roundf(objPos.x);
		objPos.y = roundf(objPos.y);
		objPos.x += FLAG_CENTER.x;
		objPos.y += FLAG_CENTER.y;
		objPos.y = fabsf (objPos.y - [_imageOrig extent].size.height);
		if ( objPos.x == locationInChart.x && objPos.y == locationInChart.y && _flag_select != obj && _flag_selected != obj){
			return obj;
		}
		i++;
	}
	return nil;
}

- (void)deletePoint:(NSPoint)locationInChart
{
	CIImage * img = [self findPoint:locationInChart];
	if (!img) return;
	unsigned i = [_plotedImages indexOfObject:img];
	if (_flag_selected) {
		if ([_plotedImages objectAtIndex:i] == _flag_selected) {
			[_plotedImages removeObjectAtIndex:i];
			_flag_selected = nil;
		} else if ([_plotedImages objectAtIndex:i-1] == _flag_selected) {
			i--;
			[_plotedImages removeObjectAtIndex:i];
			_flag_selected = nil;
		}
	}
	[_plotedImages removeObjectAtIndex:i];
	if (_flag_select) {
		[_plotedImages removeObject:_flag_select];				
		_flag_select = nil;
	}
}

// Return new location in Chart
- (NSPoint)movePointFrom:(NSPoint)depInChart to:(NSPoint)destInView
{
	[self deletePoint:depInChart];
	return [self plotPoint:destInView];
}

- (void)translatePoint:(NSPoint)p withDx:(float)x withdy:(float)y
{
	CIImage * old = [self findPoint:p];
	CIImage * new = [old imageByApplyingTransform:CGAffineTransformMakeTranslation(x,-y)];
	[_plotedImages replaceObjectAtIndex:[_plotedImages indexOfObject:old] withObject:new];
}

#pragma mark -
#pragma mark Buttons Management

- (void)performButtonAdd
{
	[_controller pointEvent:[self plotPoint:_lensCenter]];
	[self bringLensToPoint:NSMakePoint(_lensCenter.x + 10, _lensCenter.y + 10)];
}

- (void)performButtonRecenter
{
	if (_flag_selected) {
		NSPoint orig = makeNSPointFromCGPoint([_flag_selected extent].origin);
		orig.x += FLAG_SELECT_CENTER.x;	
		orig.y += FLAG_SELECT_CENTER.y;
		orig.y = fabsf(orig.y - [_imageOrig extent].size.height);
		NSPoint new = [self movePointFrom:orig to:_lensCenter];
		[(ChartDocumentController*)_controller movePointFrom:orig to:new];
		[[_buttons objectForKey:@"recenter"] setCurrentImage:@"disabled"];
		[[_buttons objectForKey:@"remove"] setCurrentImage:@"disabled"];
		[self selectPoint:new];
	}
}

- (void)performButtonDelete
{
	if (_flag_selected) {
		NSPoint objPos = makeNSPointFromCGPoint([_flag_selected extent].origin);
		objPos.x += FLAG_SELECT_CENTER.x;	
		objPos.y += FLAG_SELECT_CENTER.y;
		objPos.y = fabsf(objPos.y - [_imageOrig extent].size.height);
		[self deletePoint:objPos];
		[_controller deletePoint:objPos];
		[[_buttons objectForKey:@"recenter"] setCurrentImage:@"disabled"];
		[[_buttons objectForKey:@"remove"] setCurrentImage:@"disabled"];
	}
}

- (id)buttonUnderPoint:(NSPoint)p
{
	NSEnumerator * enu = [_buttons objectEnumerator];
	id obj;
	while (obj = [enu nextObject]) {
		NSPoint objPos = [obj position];
		NSSize objSize = makeNSSizeFromCGSize([[(Button *)obj image] extent].size);
		if (isPointInArea(p,objPos,objSize))
			return obj;
	}
	return nil;
}

- (float)maxButtonDiag
{
	float retValue = 0.;
	NSEnumerator *enu = [_buttons objectEnumerator];
	Button * btn;
	while (btn = [enu nextObject]){
		CGSize osize = [[btn image] extent].size;
		retValue = max(sqrtf(powf (osize.width, 2) + powf (osize.height, 2)), retValue);
	}
	return retValue;
}

- (void)distributeButtonsOnCircleAround:(NSPoint)aLoc center:(NSPoint)aCenter width:(float)aWidth
{
	Button * btn = nil;
	NSArray * buttons = [[_buttons objectEnumerator] allObjects];
	int btnCount = [buttons count];
	float btnRadius = [self maxButtonDiag] / 2;
	int i = 0;
	while (i < btnCount/2){
		if (btnCount % 2 != 0 && i + 1 >= btnCount / 2){
			btnRadius *= 2;
		}
		NSPoint newPos;
		// We have to find the intersection between the circle on which we
		// want to place the buttons and the cirlce around aLoc and with 
		// btnRadius radius.
		float a = (powf(aWidth,2) * 2 - powf(btnRadius,2))  / (2 *aWidth);
		float h = sqrtf (powf(aWidth,2) - powf(a,2));
		NSPoint P2;
		P2.x = aCenter.x + a * (aLoc.x - aCenter.x) / aWidth;
		P2.y = aCenter.y + a * (aLoc.y - aCenter.y) / aWidth;
		newPos.x = P2.x + h * (aLoc.y - aCenter.y) / aWidth;
		newPos.y = P2.y - h * (aLoc.x - aCenter.x) / aWidth;
		btn = [buttons objectAtIndex:i];
		[btn setPosition:newPos];
		newPos.x = P2.x - h * (aLoc.y - aCenter.y) / aWidth;
		newPos.y = P2.y + h * (aLoc.x - aCenter.x) / aWidth;		
		btn = [buttons objectAtIndex:(btnCount - 1 - i)];
		[btn setPosition:newPos];
		i++;
	}
	if (btnCount % 2 != 0) {
		btn = [buttons objectAtIndex:i];
		[btn setPosition:aLoc];
	}
}

- (NSSize)calcRectSizeWhichContainsButtons
{
	NSSize value;
	value.height = 0.;
	value.width = 0.;
	NSPoint orig = NSMakePoint(MAXFLOAT,MAXFLOAT);
	NSEnumerator *enu = [_buttons objectEnumerator];
	Button * btn;
	while (btn = [enu nextObject]){
		NSPoint opos = [btn position];
		CGSize osize = [[btn image] extent].size;
		orig.x = min(opos.x, orig.x);
		orig.y = min(opos.y, orig.y);
		value.width = max (value.width, opos.x - orig.x + osize.width);
		value.height = max (value.height, opos.y - orig.y + osize.height);
	}
	return value;
}

#pragma mark -
#pragma mark Events handling

- (void)mouseUp:(NSEvent *)theEvent
{
	if (_drag) {
		[[NSCursor arrowCursor] set];
		_drag = NO;
		
		if (_beforeDragPos.x == _lensCenter.x && _beforeDragPos.y == _lensCenter.y) {
			NSPoint mouseLoc = [self convertPoint:[theEvent locationInWindow] fromView:nil];
			if (isPointInCircle(mouseLoc,_lensCenter,[[_lens valueForKey:@"inputWidth"] floatValue] / 2)) {
				[NSCursor closedHandCursor];
				_drag = YES;
				[self bringLensToPoint:scalePointWithFactor(mouseLoc,_lensCenter,1/_magnifyScale)];
				[self setNeedsDisplay:YES];
			}
		}
	} else {
		NSPoint mouseLoc = [self convertPoint:[theEvent locationInWindow] fromView:nil];
		Button * btn = [self buttonUnderPoint:mouseLoc];
		if (btn) {
			[btn setCurrentImage:@"normal"];
			[self setNeedsDisplay:YES];
		}
	}
}

- (void)mouseDown:(NSEvent *)theEvent
{
	[[self window] makeFirstResponder:self];
	NSPoint mouseLoc = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	Button * btn = [self buttonUnderPoint:mouseLoc];
	if (btn) {
	// Click on button
		[btn setCurrentImage:@"down"];
		[btn performAction];
		[self setNeedsDisplay:YES];
	} else {
		BOOL mouseInLens = NO;
		NSPoint correctedMousePos = mouseLoc;
		if (isPointInCircle(mouseLoc, _lensCenter, [[_lens valueForKey:@"inputWidth"] floatValue] / 2 + [[_lens valueForKey:@"inputRingWidth"] floatValue])) {
			// The mouse is in lens zoom area
			correctedMousePos = scalePointWithFactor(mouseLoc,_lensCenter,1/_magnifyScale);
			mouseInLens = YES;
			_lensMoveCancel = YES;
		}
		correctedMousePos.x -= _marginsWidth;
		correctedMousePos.x /= _imageScale;
		correctedMousePos.y -= _marginsWidth;
		correctedMousePos.y /= _imageScale;

		BOOL flagSelectedChanged = NO;
		unsigned i = 0;
		unsigned nbObjs = [_plotedImages count];
		CIImage * obj;
		while (i < nbObjs){
			obj = [_plotedImages objectAtIndex:i];
			CGPoint objPos = [obj extent].origin;
			if (isPointInCircle(correctedMousePos,NSMakePoint(objPos.x + FLAG_CENTER.x,objPos.y + FLAG_CENTER.y),FLAG_CENTER.x)) {
				// Correct the obj pos to the center
				objPos.x += FLAG_CENTER.x;
				objPos.y += FLAG_CENTER.y;
				flagSelectedChanged = YES;
				[_controller selectPoint:NSMakePoint(roundf(objPos.x), roundf(fabsf(objPos.y - [_imageOrig extent].size.height)))];
				break;
			}
			i++;
			nbObjs = [_plotedImages count];
		}
		if (!flagSelectedChanged) {
			[NSCursor closedHandCursor];
			if (mouseInLens)
				_beforeDragPos = _lensCenter;
			else {
				[self bringLensToPoint:mouseLoc];
			}
			_drag = YES;
			[self mouseDragged:theEvent];
		}
	}
}

- (void)mouseDragged:(NSEvent *)theEvent
{
	if (_drag) {
		NSPoint oldPos = _lensCenter;

		// We display the closed hand cursor
		if ([NSCursor currentCursor] != [NSCursor closedHandCursor])
			[[NSCursor closedHandCursor] set];
			
		NSPoint mousePos = [self convertPoint:[theEvent locationInWindow] fromView:nil];
		NSRect selfRect = _visibleRect;
		if (mousePos.x > selfRect.size.width + selfRect.origin.x || mousePos.y > selfRect.size.height + selfRect.origin.y || mousePos.x < selfRect.origin.x || mousePos.y < selfRect.origin.y) {
			// the mouse is outside the view : we can't directly use the mouse position delta
			// but we use the saved offset and correct the mouse position
			_lensCenter.x = mousePos.x + _lensMouseDragDelta.width;
			_lensCenter.y = mousePos.y + _lensMouseDragDelta.height;
		} else {
			// the mouse is inside the view : we just have to move the lens with the mouse deplacement
			// and save the offset between  the lens and the mouse
			_lensMouseDragDelta.width = _lensCenter.x - mousePos.x;
			_lensMouseDragDelta.height = _lensCenter.y - mousePos.y;
			_lensCenter.x += [theEvent deltaX];
			_lensCenter.y -= [theEvent deltaY];
		}
		// sending autoscroll
		[self autoscroll:theEvent];
		
		// we calculate the redraw zone to avoid redrawind the total surface of the view
		float width = [[_lens valueForKey:@"inputWidth"] floatValue] / 2 + [[_lens valueForKey:@"inputRingWidth"] floatValue];
		NSRect redrawZone;
		NSSize buttonArea = [self calcRectSizeWhichContainsButtons];
		redrawZone.origin.x = ( _lensCenter.x < oldPos.x ? _lensCenter.x : oldPos.x) - (width + buttonArea.width * 2);
		redrawZone.origin.y = ( _lensCenter.y < oldPos.y ? _lensCenter.y : oldPos.y) - (width + buttonArea.height * 2);
		redrawZone.size.width = fabsf (_lensCenter.x - oldPos.x) + (width * 2 + buttonArea.width * 4);
		redrawZone.size.height = fabsf (_lensCenter.y - oldPos.y) + (width * 2 + buttonArea.height * 4);
		[self setNeedsDisplayInRect:redrawZone];
	}
}

- (void)mouseMoved:(NSEvent *)theEvent
{
	NSPoint mouseLoc = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	
	// we check if the mouse is over a button
	// and change its represntation if needed 
	Button * btnUnderMouse = [self buttonUnderPoint:mouseLoc];
	Button * btn;
	NSEnumerator * enu = [_buttons objectEnumerator];
	while (btn = [enu nextObject]) {
		if (btn == btnUnderMouse) {
			[btn setCurrentImage:@"up"];
		} else {
			[btn setCurrentImage:@"normal"];
		} 
	}
	if (!_flag_selected) {
		[[_buttons objectForKey:@"recenter"] setCurrentImage:@"disabled"];
		[[_buttons objectForKey:@"remove"] setCurrentImage:@"disabled"];
	}
		
	float width = [[_lens valueForKey:@"inputWidth"] floatValue] / 2 + [[_lens valueForKey:@"inputRingWidth"] floatValue];
	
	// We have to rescale the mouse position and correct margins and lens distortion
	if (isPointInCircle(mouseLoc, _lensCenter, width)) {
		// The mouse is in lens zoom area
		mouseLoc = scalePointWithFactor(mouseLoc,_lensCenter,1/_magnifyScale);
	}

	
	mouseLoc.x -= _marginsWidth;
	mouseLoc.x /= _imageScale;
	mouseLoc.y -= _marginsWidth;
	mouseLoc.y /= _imageScale;

	unsigned i = 0;
	unsigned nbObjs = [_plotedImages count];
	CIImage * obj;
	while (i < nbObjs){
		// update the selecting feedback
		obj = [_plotedImages objectAtIndex:i];
		CGPoint objPos = [obj extent].origin;
		if (isPointInCircle(mouseLoc,NSMakePoint(objPos.x + FLAG_CENTER.x,objPos.y + FLAG_CENTER.y),FLAG_CENTER.x) && _flag_select != obj && _flag_selected != obj){
			// we add the selecting feedback if the mouse is over a flag
			[_plotedImages removeObject:_flag_select];
			_flag_select = [FLAG_SELECT	imageByApplyingTransform:
				CGAffineTransformMakeTranslation(objPos.x + FLAG_CENTER.x - FLAG_SELECT_CENTER.x, objPos.y  + FLAG_CENTER.y - FLAG_SELECT_CENTER.y)];
			if (i > 0 && _flag_selected == [_plotedImages objectAtIndex:i-1]) {
				// We Want to keep the Flag Selected just before de corresponding point
				[_plotedImages insertObject:_flag_select atIndex:i-1];
			} else {
				[_plotedImages insertObject:_flag_select atIndex:i];
			}
			i++;
			break;
		} else {
			if (_flag_select) {
			// the mouse is not over the flag : we remove the user feedback
				[_plotedImages removeObject:_flag_select];
				_flag_select = nil;
			} else {
				i++;
			}
		}
		nbObjs = [_plotedImages count];
	}
	[self setNeedsDisplay:YES];
}


- (void)keyDown:(NSEvent *)theEvent
{
	switch ([[theEvent characters] characterAtIndex:0]) {
	case NSUpArrowFunctionKey:
		_lensCenter.y += 1 / _magnifyScale;
		break;
	case NSDownArrowFunctionKey:
		_lensCenter.y -= 1 / _magnifyScale;
		break;
	case NSLeftArrowFunctionKey:
		_lensCenter.x -= 1 / _magnifyScale;
		break;
	case NSRightArrowFunctionKey:
		_lensCenter.x += 1 / _magnifyScale;
		break;
	case NSDeleteFunctionKey:
	case '\177':
		[self performButtonDelete];
		break;
	case ' ':
		[self performButtonAdd];
		break;
	default:
		break;
	}
	[self setNeedsDisplay:YES];
}

- (void)selectPoint:(NSPoint)p
{
	// converts p to the view and displays selection of the corresponding point

	NSPoint pLoc = p;
	pLoc.x -= FLAG_CENTER.x;
	pLoc.y += FLAG_CENTER.y;
	pLoc.y = - (pLoc.y - [_imageOrig extent].size.height);
	
	if (_flag_selected)
		[_plotedImages removeObject:_flag_selected];
	_flag_selected = nil;
	
	[[_buttons objectForKey:@"recenter"] setCurrentImage:@"disabled"];
	[[_buttons objectForKey:@"remove"] setCurrentImage:@"disabled"];

	unsigned i = 0;
	unsigned nbObjs = [_plotedImages count];
	CIImage * obj;
	while (i < nbObjs){
		obj = [_plotedImages objectAtIndex:i];
		CGPoint objPos = [obj extent].origin;
		objPos.x = roundf(objPos.x);
		objPos.y = roundf(objPos.y);
		if ( objPos.x == pLoc.x && objPos.y == pLoc.y && _flag_select != obj && _flag_selected != obj){
			_flag_selected = [FLAG_SELECT	imageByApplyingTransform:
				CGAffineTransformMakeTranslation(objPos.x + FLAG_CENTER.x - FLAG_SELECT_CENTER.x, objPos.y  + FLAG_CENTER.y - FLAG_SELECT_CENTER.y)];
			[_plotedImages insertObject:_flag_selected atIndex:i];
			[[_buttons objectForKey:@"recenter"] setCurrentImage:@"normal"];
			[[_buttons objectForKey:@"remove"] setCurrentImage:@"normal"];
			[self bringLensToPoint:[self convertPointForZoomedView:p]];
			break;
		}
		i++;
		nbObjs = [_plotedImages count];
	}
	
	NSRect visibleRect = _visibleRect;
	
	p.x *= _imageScale;
	p.x += _marginsWidth;
	p.y *= _imageScale;
	p.y += _marginsWidth;
	p.y = -(p.y - ([self frame].size.height));

	if (!isPointInRect(p,visibleRect)) {
		p.x -= visibleRect.size.width / 2;
		p.y -= visibleRect.size.height / 2;
		[self scrollPoint:p];
	}
	
	[self setNeedsDisplay:YES];
	
}

- (void)deSelectPoint
{
	[_plotedImages removeObject:_flag_selected];
	_flag_selected = nil;
}


- (void)setUsesTransparency:(BOOL)b forPoint:(NSPoint)p
{
	id old = [self findPoint:p];
	if (!old) return;
	CGPoint oldOrig = [old extent].origin;
	CIImage * new;
	if (b) {
		new = [FLAG_TRANSPARENT imageByApplyingTransform:CGAffineTransformMakeTranslation(oldOrig.x, oldOrig.y)];
	} else {
		new = [FLAG imageByApplyingTransform:CGAffineTransformMakeTranslation(oldOrig.x, oldOrig.y)];
	}
	[_plotedImages replaceObjectAtIndex:[_plotedImages indexOfObject:old] withObject:new];
	[self setNeedsDisplay:YES];
}


#pragma mark -
#pragma mark Lens Movement


-(void)incrementMovement
{
	NSPoint p = _lensDestination;
	if (fabsf (p.x - _lensCenter.x) > 2. ||  fabsf (p.y - _lensCenter.y) > 2.) {
		_lensCenter.x += (p.x - _lensCenter.x) / 3;
		_lensCenter.y += (p.y - _lensCenter.y) / 3;
		[self setNeedsDisplay:YES];
		struct timespec rqtp;
		rqtp.tv_sec = 0;
		rqtp.tv_nsec = 70000000;
		nanosleep(&rqtp,NULL);
		if (!_lensMoveCancel)
			[self incrementMovement];
	} else {
		_lensCenter.x = p.x;
		_lensCenter.y = p.y;
		[self setNeedsDisplay:YES];
	}

}

-(void)launchLensMovemmentThread:(id)parameters;
{
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	[self incrementMovement];
	_lensMoveLaunched = NO;
	[parameters release];
	[pool release];
}

-(void)bringLensToPoint:(NSPoint)p
{
	_lensDestination = p;
	_lensMoveCancel = NO;
	if (!_lensMoveLaunched) {
		[NSThread detachNewThreadSelector:@selector(launchLensMovemmentThread:) 
			toTarget:self 
			withObject:nil];
	}
}

#pragma mark -
#pragma mark Drawing method

- (void)drawRect:(NSRect)rect
{
	// Makes CoreImage Draw the specified rectangle
	CGRect cg;
	NSRect r;
	r = rect;

	cg = CGRectIntegral (*(CGRect *)&r);
	
	// Button position calculation
	NSSize imgSize = _visibleRect.size;
	NSPoint imgOrig = _visibleRect.origin;
	imgSize.width += imgOrig.x;
	imgSize.height += imgOrig.y;
	
	float lensRadius = [[_lens valueForKey:@"inputWidth"] floatValue] / 2 + [[_lens valueForKey:@"inputRingWidth"] floatValue] + [self maxButtonDiag] / 2;

	// init button pos with normal position
	float buttonXpos = _lensCenter.x + sinf (pi/4) * lensRadius;
	float buttonYpos = _lensCenter.y + sinf (-pi/4) * lensRadius;

	[self distributeButtonsOnCircleAround:NSMakePoint(buttonXpos,buttonYpos) center:_lensCenter width:lensRadius];
/*	
	This code doesn't work but is kept as archive

	NSSize btnSize = [self calcRectSizeWhichContainsButtons];
	// button postion correction at edges
	BOOL minX = NO, maxX = NO, minY = NO, maxY = NO;
	if ((buttonXpos + btnSize.width / 2) >= imgSize.width) {
		buttonXpos = imgSize.width - btnSize.width / 2;
		maxX = YES;
	} else if(buttonXpos <= btnSize.width / 2 + imgOrig.x) {
		buttonXpos = btnSize.width / 2 + imgOrig.x;
		minX = YES; 
	}
	if ((buttonYpos + btnSize.height / 2) >= imgSize.height){
		buttonYpos = imgSize.height - btnSize.height / 2;
		maxY = YES;	
	} else if (buttonYpos <= btnSize.height / 2 + imgOrig.y) {
		buttonYpos = btnSize.height / 2 + imgOrig.y;
		minY = YES;	
	}
	
	// redistribution with new values
	[self distributeButtonsOnCircleAround:NSMakePoint(buttonXpos,buttonYpos) center:_lensCenter width:lensRadius];
	btnSize = [self calcRectSizeWhichContainsButtons];
	NSPoint distFromCenter;
	distFromCenter.x = (_lensCenter.x - buttonXpos);
	distFromCenter.y = (_lensCenter.y - buttonYpos);
	if (sqrtf(powf(distFromCenter.x, 2) + powf(distFromCenter.y, 2)) < lensRadius) {
		if (minY) {
			buttonXpos = sqrtf (powf(lensRadius, 2) - powf(distFromCenter.y, 2)) + _lensCenter.x;
		}
		
		[self distributeButtonsOnCircleAround:NSMakePoint(buttonXpos,buttonYpos) center:_lensCenter width:lensRadius];
		btnSize = [self calcRectSizeWhichContainsButtons];
		minX = maxX = NO;
		if ((buttonXpos + btnSize.width / 2) >= imgSize.width) {
			buttonXpos = imgSize.width - btnSize.width / 2;
			maxX = YES;
		} else if(buttonXpos <= btnSize.width / 2 + imgOrig.x) {
			buttonXpos = btnSize.width / 2 + imgOrig.x;
			minX = YES; 
		}
		
		distFromCenter.x = (_lensCenter.x - buttonXpos);
		distFromCenter.y = (_lensCenter.y - buttonYpos);
		
		if (maxX && !minY) {
			buttonYpos = _lensCenter.y - sqrtf (powf(lensRadius, 2) - powf(distFromCenter.x, 2));
		}
		
		[self distributeButtonsOnCircleAround:NSMakePoint(buttonXpos,buttonYpos) center:_lensCenter width:lensRadius];
		btnSize = [self calcRectSizeWhichContainsButtons];
		if ((buttonYpos + btnSize.height / 2) >= imgSize.height){
			buttonYpos = imgSize.height - btnSize.height / 2;
			maxY = YES;	
		} else if (buttonYpos <= btnSize.height / 2 + imgOrig.y) {
			buttonYpos = btnSize.height / 2 + imgOrig.y;
			minY = YES;	
		}
		
		distFromCenter.x = (_lensCenter.x - buttonXpos);
		distFromCenter.y = (_lensCenter.y - buttonYpos);
		
		if (maxX && minY) {
			distFromCenter.y = sqrtf (powf(lensRadius, 2) - powf(distFromCenter.x, 2));
			buttonYpos = _lensCenter.y + distFromCenter.y;
		}
		
	}

	if (!isPointInCircle(NSMakePoint(buttonXpos,buttonYpos),_lensCenter,lensRadius + 5)) {
		// buttons are too far from lens
		buttonXpos = _lensCenter.x + sinf (pi/4) * lensRadius;
		buttonYpos = _lensCenter.y + sinf (-pi/4) * lensRadius;
	}

	[self distributeButtonsOnCircleAround:NSMakePoint(buttonXpos,buttonYpos) center:_lensCenter width:lensRadius];
*/
    CIContext *context = [[NSGraphicsContext currentContext] CIContext];
		
	[_lens setValue:[CIVector vectorWithX:_lensCenter.x Y:_lensCenter.y] forKey:@"inputCenter"];

	CIImage * result = _imageOrig;	
	
	NSEnumerator * imagesEnum = [_plotedImages objectEnumerator];
	id obj;
	while (obj = [imagesEnum nextObject]){
		[_compose setValue:obj forKey:@"inputImage"];
		[_compose setValue:result forKey:@"inputBackgroundImage"];
		result = [_compose valueForKey:@"outputImage"];
	}
	
	[_scale setValue:result forKey:@"inputImage"];
	result = [_scale valueForKey:@"outputImage"];
	
	result = [self placeImage:result];
	[_image autorelease];
	_image = result;
	[_image retain];



	[_lens setValue:result forKey:@"inputImage"];
	result = [_lens valueForKey:@"outputImage"];
	
	NSEnumerator * enu = [_buttons objectEnumerator];
	Button * btn;
	while (btn = [enu nextObject]) {
		NSPoint buttonCenter = [btn position];
		CGSize btnSize = [[btn image] extent].size;
		[_compose setValue:[[btn image] imageByApplyingTransform:
			CGAffineTransformMakeTranslation (buttonCenter.x - btnSize.width / 2, buttonCenter.y -  btnSize.height / 2)] forKey:@"inputImage"];
		[_compose setValue:result forKey:@"inputBackgroundImage"];
		result = [_compose valueForKey:@"outputImage"];
	}

    [context drawImage:result atPoint:cg.origin fromRect:cg];
}

#pragma mark -

- (void) dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[_imageOrig release];
	[_image release];
	[_lens release];
	[_compose release];
	[_scale release];
	[_plotedImages release];
	[super dealloc];
}

@end
