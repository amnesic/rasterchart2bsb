/* ChartDocumentController */

//  Created by Nicolas Cherel.
//  Copyright 2005 Magic Instinct Software. 
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#import <Cocoa/Cocoa.h>
#import "ChartData.h"

#import "ChartViewProtocol.h"
#import "ChartViewCGProtocol.h"

enum {
	LensEffectNotUsed = 1,
	LensEffectUsed = 2,
	LensEffectUnknown = 3
};

@interface ChartDocumentController : NSObject
{
    IBOutlet NSTextField 	*_bsbVer;
    IBOutlet NSTextField 	*_chartName;
    IBOutlet NSTextField 	*_dpi;
    IBOutlet NSTextField 	*_editionDate;
    IBOutlet NSTextField 	*_geoDatum;
    IBOutlet NSTextField 	*_projectionParameter;
    IBOutlet NSComboBox 	*_projection;
    IBOutlet NSTextField 	*_scale;
    IBOutlet NSScrollView 	*_scrollView;
    IBOutlet NSTextField 	*_skewAngle;
    IBOutlet NSComboBox 	*_soundingDatum;
    IBOutlet NSComboBox		*_units;
    IBOutlet NSTableView 	*_pointsTable;
	IBOutlet NSPopUpButton	*_zoomPopUp;
	IBOutlet NSSlider		*_lensMagSlider;

	NSImage					*_imageHoldBack;
	ChartData 				*_chartData;
		
// Point coordinates edition handling
	BOOL					_coordsEdited;
	NSPoint					_lastCoords;
	
	int						_usesLensEffect;
	BOOL					_selectionDidChangeWillBeCalledTwice;
}

- (void)setImageData:(NSData *)data withFilePath:(NSString *)_filePath;
- (void)setImageData:(NSData *)data;

- (IBAction)setBSBVersion:(id)sender;
- (IBAction)setChartName:(id)sender;
- (IBAction)setDPI:(id)sender;
- (IBAction)setEditDate:(id)sender;
- (IBAction)setGeodeticDatum:(id)sender;
- (IBAction)setImageHeight:(id)sender;
- (IBAction)setImageWidth:(id)sender;
- (IBAction)setLensZoom:(id)sender;
- (IBAction)setProjection:(id)sender;
- (IBAction)setProjectionParameter:(id)sender;
- (IBAction)setScale:(id)sender;
- (IBAction)setSkewAngle:(id)sender;
- (IBAction)setSoundingDatum:(id)sender;
- (IBAction)setUnit:(id)sender;
- (IBAction)setZoom:(id)sender;
// IBOutlet : NSPopUpButton Workaround
- (IBAction)zoomPopUpWarpper:(id)sender;

- (void)pointEvent:(NSPoint)p;
- (void)selectPoint:(NSPoint)p;
- (void)deletePoint:(NSPoint)p;
- (void)movePointFrom:(NSPoint)dep to:(NSPoint)dest;

- (void)initAllData;

- (ChartData *)chartData;
- (NSView *)imageView;

@end

@interface NSUserDefaults(ImageEffectsPreferences)
- (void)setImageLensPreference:(NSString *)filepath withLensState:(int)l;
- (int)imageLensPreference:(NSString *)filepath;
@end
