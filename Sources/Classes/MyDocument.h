//
//  MyDocument.h
//  RasterChart2BSB
//
//  Created by Nicolas Cherel on 04/07/05.
//  Copyright 2005 Magic Instinct Software.  
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

//


#import <Cocoa/Cocoa.h>
#import "ChartDocumentController.h"
@interface MouseMovedTransmitter : NSResponder
{
	NSResponder * _responder;
}
- (id)initWithResponder:(NSResponder *)responder;
- (void)setResponder:(NSResponder *)responder;

@end

@interface MyDocument : NSDocument
{
	IBOutlet ChartDocumentController 	* _controller;
	IBOutlet NSPanel					* _exportProgressPanel;
	IBOutlet NSProgressIndicator		* _progressionBar;
	IBOutlet NSProgressIndicator		* _progressionSpin;
	IBOutlet NSTextField				* _progressText;

	NSData							 	* _data;
	NSView 								* _openPanelAccessory;
	NSString							* _filePath;
	
	BOOL					_documentCanBeClosed;

// window's next responder
	MouseMovedTransmitter	* _mouseMovedResponder;

// saveToURL parameters
	NSURL 					* _absoluteURL;
	NSString 				* _typeName;
	NSSaveOperationType		_saveOperation; 
	id						_delegate;
	SEL						_didSaveSelector;
	void					* _contextInfo;

}
@end

