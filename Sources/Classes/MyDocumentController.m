
//  Created by Nicolas Cherel.
//  Copyright 2005 Magic Instinct Software. 
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#import "MyDocumentController.h"
#import "OpenPanelAccessory.h"


@implementation MyDocumentController

-(void)refreshWelcomePopUP
{
	[_recentList removeAllItems];
	NSMenuItem * item = [[[NSMenuItem alloc] init] autorelease];
	[item setTitle:@"Browse..."];
	[item setTarget:self];
	[item setAction:@selector(openFileOutlet:)];
	[[_recentList menu] addItem:item];
	[[_recentList menu] addItem:[NSMenuItem separatorItem]];
	NSEnumerator * enu =  [[self recentDocumentURLs] objectEnumerator];
	id obj;
	while (obj = [enu nextObject]) {
		item = [[[NSMenuItem alloc] init] autorelease];
		[item setTarget:self];
		[item setAction:@selector(openRecentOutlet:)];
		[item setRepresentedObject:obj];
		[item setTitle:[obj path]];
		[[_recentList menu] addItem:item];
	}
}

- (void)displayWelcomePopUp
{
	if (!_applicationWillTerminate) {
		[self refreshWelcomePopUP];
		[_welcomeWindow orderFront:self];
	}
}

-(void)awakeFromNib
{
	NSColor *color = [NSColor blueColor];

	NSMutableAttributedString *colorTitle =
	[[NSMutableAttributedString alloc] initWithAttributedString:[_urlButton attributedTitle]];
	NSRange titleRange = NSMakeRange(0, [colorTitle length]);
	[colorTitle addAttribute:NSForegroundColorAttributeName	value:color	range:titleRange];
	[colorTitle addAttribute:NSUnderlineStyleAttributeName	value:[NSNumber numberWithInt:1] range:titleRange];
	[_urlButton setAttributedTitle:colorTitle];

	[_imageLogo setImage:[[NSImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Logo" ofType:@"png"]]];
	[self refreshWelcomePopUP];
}

-(IBAction)openFileOutlet:(id)sender
{
	[self openDocument:self];
}

-(IBAction)openRecentOutlet:(id)sender;
{
	[self openDocumentWithContentsOfURL:[[_recentList selectedItem] representedObject] display:YES];
}

- (void)addDocument:(NSDocument *)document
{
	[_welcomeWindow orderOut:self];
	[super addDocument:document];
}

- (BOOL)applicationShouldOpenUntitledFile:(NSApplication *)sender
{
 	return NO;
}

- (id)init
{
	self = [super init];
	if (self != nil) {
		_OPanelAccessory = [[OpenPanelAccessory alloc] init];
	}
	_prefChanged = NO;
	return self;
}

- (int)runModalOpenPanel:(NSOpenPanel *)openPanel forTypes:(NSArray *)extensions
{
	[(id)_OPanelAccessory setPrefChanged];
	[openPanel setAccessoryView:_OPanelAccessory];
	return [openPanel runModalForTypes:extensions];
}

- (BOOL)userWantsLensEffect
{
	return [(id)_OPanelAccessory lensEffectSelected];
}

- (BOOL)userChangedPreferencies
{
	return [(id)_OPanelAccessory prefChanged];
}

- (void)resetUserChangedPreferencies
{
	[(id)_OPanelAccessory resetPrefChanged];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification
{
	_applicationWillTerminate = YES;
}

-(IBAction)openJustMagicURL:(id)sender
{
	[[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://www.justmagic.com"]];
}

- (void)dealloc
{
	[_OPanelAccessory release];
	[super dealloc];
}


@end
