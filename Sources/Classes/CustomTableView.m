
//  Created by Nicolas Cherel.
//  Copyright 2005 Magic Instinct Software. 
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#import "CustomTableView.h"


@interface TableViewDelegate : NSObject
@end

@implementation TableViewDelegate
- (BOOL)selectionShouldChangeInTableView:(NSTableView *)aTableView
{
	if ([aTableView editedRow] >= 0) return NO;
	else return YES;
}
@end



// We subclass CustomTableView to redefine Enter Key behaviour
@implementation CustomTableView

-(void)awakeFromNib
{
	[self setDelegate:[[TableViewDelegate alloc] init]];
}

// Code from http://cocoa.mamasam.com/COCOADEV/2002/01/2/22208.php
// Author Kurt Revis 
- (void)textDidEndEditing:(NSNotification *)notification 
{ 
	int movement = [[[notification userInfo] objectForKey:@"NSTextMovement"] intValue];
    if ( movement == NSReturnTextMovement || movement == NSTabTextMovement) {
        // This is ugly, but just about the only way to do it. 
		// NSTableView is determined to select and edit something else, even the 
		// text field that it just finished editing, unless we mislead it about 
		// what key was pressed to end editing. 
		int mEditedRow = [self editedRow];
		int mEditedColumn = [self editedColumn];
		
		NSMutableDictionary *newUserInfo; 
		NSNotification *newNotification; 

        newUserInfo = [NSMutableDictionary dictionaryWithDictionary:[notification userInfo]]; 
		[newUserInfo setObject:[NSNumber numberWithInt:NSIllegalTextMovement] forKey:@"NSTextMovement"]; 
		newNotification = [NSNotification notificationWithName:[notification name] object:[notification object] userInfo:newUserInfo];
		[super textDidEndEditing:newNotification]; 
		
		if (mEditedColumn < [self numberOfColumns] - 1 )
			[self editColumn:mEditedColumn+1 row:mEditedRow withEvent:nil select:YES];
//		else 
//			[self selectRow:mEditedRow byExtendingSelection:NO];
//		[[self window] makeFirstResponder:self];

     } else { 
         [super textDidEndEditing:notification]; 
     }
}

-(void)keyDown:(NSEvent *)theEvent
{
	switch ([[theEvent characters] characterAtIndex:0]) {
	case NSDeleteFunctionKey:
	case NSBackspaceCharacter:
	case '\177':
		[[self window] keyDown:theEvent];
		break;
	default:
		[super keyDown:theEvent];
		break;
	}
}

- (void) dealloc {
	[[self delegate] release];
	[super dealloc];
}


@end
