//
//  Button.m
//  RasterChart2BSB
//
//  Created by Nicolas Cherel on 28/07/05.
//  Copyright 2005 Magic Instinct Software. 
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#import <ExceptionHandling/NSExceptionHandler.h>
#import "Button.h"


@implementation Button
+ (id)buttonWithImages:(NSDictionary *)theImages withAction:(SEL)anAction onObject:(id)anObject
{
	id obj = [[self alloc] initWithImages:theImages withAction:anAction onObject:anObject];
	return [obj autorelease];
}

- (id)initWithImages:(NSDictionary *)theImages withAction:(SEL)anAction onObject:(id)anObject
{
	[super init];
	_images = [theImages retain];
	_action = anAction;
	_object = [anObject retain];
	return self;
}

- (void)performAction
{
	[_object performSelector:_action]; 
}

- (void)setCurrentImage:(NSString *)s
{
	_image = [_images objectForKey:s];
	if (!_image) {
		@throw [NSException exceptionWithName:@"ExceptionImageNotKnown" 
			reason:[NSString stringWithFormat:@"%s,%d : The image name can't be found",
			rindex (__BASE_FILE__, '/') == NULL ? __BASE_FILE__ : &rindex (__BASE_FILE__, '/')[1],
			__LINE__, __FUNCTION__]
			userInfo:[NSDictionary dictionary]];
	}
}

- (id)image
{
	return _image;
}

- (NSPoint)position
{
	return _position;
}

- (void)setPosition:(NSPoint) p
{
	_position = p;
}

//=========================================================== 
// dealloc
//=========================================================== 
- (void)dealloc
{
	[_images release];
    [super dealloc];
}



@end
