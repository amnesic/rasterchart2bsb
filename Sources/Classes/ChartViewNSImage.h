/* ChartViewNSImage */

//  Created by Nicolas Cherel.
//  Copyright 2005 Magic Instinct Software. 
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#import <Cocoa/Cocoa.h>

#import "ChartDocumentController.h"
#import "Button.h"

#import "ChartViewProtocol.h"

@interface ChartViewNSImage : NSImageView <ChartView>
{
	ChartDocumentController 	*_controller;
	NSTrackingRectTag 			_trackTag;
	NSImageView					*_flag_selected;
	NSImageView					*_flag_select;
	BOOL						_drag;
	NSDate						*_clock_at_drag_begin;
	NSImageView					*_dragged_obj;
	Button						*_button_remove;
	NSImageView					*_button_view;

	double						_scale;

	
// We trace difference between mouseLoc and draggedFlag Loc
	float						_deltaDragFlagX;
	float						_deltaDragFlagY;
	
	NSImage 					*FLAG;
	NSImage 					*FLAG_TRANSPARENT;
	NSPoint 					FLAG_CENTER;
	NSImageView 				*FLAG_SELECT;
	NSPoint 					FLAG_SELECT_CENTER;
}

- (id)initWithController:(ChartDocumentController *)controller;
- (void)setUsesTransparency:(BOOL)b forPoint:(NSPoint)p;
- (void)translatePoint:(NSPoint)p withDx:(float)x withdy:(float)y;
- (void)setImageZoom:(double)scale;
- (void)translatePoint:(NSPoint)p withDx:(float)x withdy:(float)y;

@end
