//
//  MyDocument.m
//  RasterChart2BSB
//
//  Created by Nicolas Cherel on 04/07/05.
//  Copyright 2005 Magic Instinct Software.  
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

//


#import "MyDocument.h"
#import "BSBExport.h"
#import "MyDocumentController.h"

@implementation MouseMovedTransmitter

- (id)initWithResponder:(NSResponder *)responder
{
	[super init];
	_responder = [responder retain];
	return self;
}

- (void)setResponder:(NSResponder *)responder
{
	[_responder release];
	_responder = [responder retain];
}

- (void)mouseMoved:(NSEvent *)theEvent
{
	[_responder mouseMoved:theEvent];
}

-(void)keyDown:(NSEvent *)theEvent
{
	switch ([[theEvent characters] characterAtIndex:0]) {
	case NSDeleteFunctionKey:
	case NSBackspaceCharacter:
	case '\177':
		[_responder keyDown:theEvent];
		break;
	}
}

- (void) dealloc {
	[_responder release];
	[super dealloc];
}

@end

@implementation MyDocument

- (id)init
{
    self = [super init];
    if (self) {
        // Add your subclass-specific initialization here.
        // If an error occurs here, send a [self release] message and return nil.
    	_data = nil;
    }
    return self;
}

- (void)observeExporter:(NSDictionary *)param
{
	[[_progressText cell] setObjectValue:[param valueForKey:@"text"]];
	[_progressionBar setDoubleValue:[[param valueForKey:@"value"] doubleValue]];
}


- (NSString *)windowNibName
{
    // Override returning the nib file name of the document
    // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this method and override -makeWindowControllers instead.
    return @"MyDocument";
}

- (void)windowControllerDidLoadNib:(NSWindowController *) aController
{
    [super windowControllerDidLoadNib:aController];
	
    [_controller setImageData:_data withFilePath:_filePath];
	if (_data) [_data release];
	_data = nil;
	[[[_controller imageView] window] setAcceptsMouseMovedEvents:YES];
	[[[_controller imageView] window] setInitialFirstResponder:[_controller imageView]];
	_mouseMovedResponder = [[MouseMovedTransmitter alloc] initWithResponder:[_controller imageView]];
	[[[_controller imageView] window] setNextResponder:_mouseMovedResponder];
	[_controller initAllData];

}


- (BOOL)canSaveDocument
{
	if ([[[_controller chartData] refPoints] count] < 3) {
		NSBeginAlertSheet(nil,nil,nil,nil,[[_controller imageView] window],nil,nil,nil,nil,@"You must plot at least three reference points to create a valid document");
		return NO;
	}
	NSEnumerator * enu = [[[_controller chartData] refPoints] objectEnumerator];
	id obj;
	while (obj = [enu nextObject]) {
		if (![obj objectForKey:@"lat"] || ![obj objectForKey:@"lon"]) {	
			NSBeginAlertSheet(nil,nil,nil,nil,[[_controller imageView] window],nil,nil,nil,nil,@"You must enter latitudes and longitudes for each reference points");
			return NO;
		}
	}
	return YES;
}

- (IBAction)saveDocument:(id)sender
{
	if (![self canSaveDocument]) return;
	return [super saveDocument:sender];
}

- (IBAction)saveDocumentAs:(id)sender
{
	if (![self canSaveDocument]) return;
	return [super saveDocumentAs:sender];
}

- (IBAction)saveDocumentTo:(id)sender
{
	if (![self canSaveDocument]) return;
	return [super saveDocumentTo:sender];
}
 
- (void)mySaveToURL
{
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
#if MAC_OS_X_VERSION_MAX_ALLOWED >= 1040
	[super saveToURL:_absoluteURL ofType:_typeName forSaveOperation:_saveOperation delegate:_delegate didSaveSelector:_didSaveSelector contextInfo:_contextInfo];
#else
	[super saveToFile:(NSString *)_absoluteURL saveOperation:_saveOperation delegate:_delegate didSaveSelector:_didSaveSelector contextInfo:_contextInfo];
#endif
	[pool release];
}

// We put saveToURL: (Called by -[NSavePanel ok:] after a -[NSDocument saveDocumentAs:]) in a other thread
#if MAC_OS_X_VERSION_MAX_ALLOWED >= 1040
- (void)saveToURL:(NSURL *)absoluteURL ofType:(NSString *)typeName forSaveOperation:(NSSaveOperationType)saveOperation delegate:(id)delegate didSaveSelector:(SEL)didSaveSelector contextInfo:(void *)contextInfo
#else 
- (void)saveToFile:(NSString *)absoluteURL saveOperation:(NSSaveOperationType)saveOperation delegate:(id)delegate didSaveSelector:(SEL)didSaveSelector contextInfo:(void *)contextInfo
#endif
{
	// we backup the parameters for mySaveToURL
#if MAC_OS_X_VERSION_MAX_ALLOWED >= 1040
	_absoluteURL = absoluteURL;
	_typeName = typeName;
#else
	_absoluteURL = (NSURL *) absoluteURL;
#endif
	_saveOperation = saveOperation; 
	_delegate = delegate;
	_didSaveSelector = didSaveSelector;
	_contextInfo = contextInfo;
	[self updateChangeCount:NSChangeCleared];
	[NSThread detachNewThreadSelector:@selector(mySaveToURL) toTarget:self withObject:nil];
}


- (NSData *)dataRepresentationOfType:(NSString *)aType
{
	NSData * returnVal = nil;
	[[_progressText cell] setObjectValue:@"Exporting initialization"];
	[NSApp beginSheet:_exportProgressPanel modalForWindow:[[_controller imageView] window] modalDelegate:nil didEndSelector:nil contextInfo:nil];

	[_progressionBar setMinValue:0.];
	[_progressionBar setMaxValue:1.];
	[_progressionBar setUsesThreadedAnimation:YES];
	[_progressionBar startAnimation:self];
	[_progressionSpin setUsesThreadedAnimation:YES];
	[_progressionSpin startAnimation:self];

	id exporter = nil;
	if ([aType isEqualToString:@"MapTech BSB Chart"]) {
		exporter = [[BSBExport alloc] initWithObserver:self andSelector:@selector(observeExporter:)];
	}
	if (exporter != nil)
		returnVal = [exporter exportReferencedImage:[NSData dataWithContentsOfFile:_filePath] withGeoData:[_controller chartData]];
	[_progressionBar stopAnimation:self];
	[_progressionSpin stopAnimation:self];
	[_exportProgressPanel orderOut:self];
	[_progressionBar setDoubleValue:0.];
	[NSApp endSheet:_exportProgressPanel];
    return returnVal;
}

- (void)close
{
	[super close];
	if ([[[NSDocumentController sharedDocumentController] documents] count] == 0) 
		[(MyDocumentController *)[NSDocumentController sharedDocumentController] displayWelcomePopUp];
}


- (BOOL)readFromFile:(NSString *)fileName ofType:(NSString *)docType
{
    // Insert code here to read your document from the given data.  You can also choose to override -loadFileWrapperRepresentation:ofType: or -readFromFile:ofType: instead.
    
    // For applications targeted for Tiger or later systems, you should use the new Tiger API readFromData:ofType:error:.  In this case you can also choose to override -readFromURL:ofType:error: or -readFromFileWrapper:ofType:error: instead.

	NSLog (docType);
	
	if ([docType isEqualToString:@"MapTech BSB Chart"]) {
		NSRunCriticalAlertPanel(@"Can't open BSB image file",@"The BSB files can't be opened yet",@"Ok",nil,nil);
		return NO;
	} else if ([docType isEqualToString:@"JPEG 2000 Image"] ||
				[docType isEqualToString:@"NeXT TIFF v4.0 pasteboard type"] ||
				[docType isEqualToString:@"Windows Bitmap Image"] ||
				[docType isEqualToString:@"Graphics Interchange Format Image"] ||
				[docType isEqualToString:@"JPEG Image"] ||
				[docType isEqualToString:@"Portable Network Graphics Image"]) {
		_data = [[NSData dataWithContentsOfFile:fileName] retain];
		_filePath = [fileName retain];
		return YES;
	} else {
		return NO;
	}

}

- (void) dealloc 
{
	[_filePath release];
	[_mouseMovedResponder release];
	[super dealloc];
}


@end
