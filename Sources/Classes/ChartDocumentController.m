
//  Created by Nicolas Cherel.
//  Copyright 2005 Magic Instinct Software. 
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#import "ChartDocumentController.h"
#import "MyDocumentController.h"
#import "ChartViewNSImage.h"
#import "ImageViewWarpper.h"


@implementation ChartDocumentController


- (ChartDocumentController *)init
{
	[super init];
	_chartData = [[ChartData alloc] init];
	_usesLensEffect = LensEffectUnknown;
	_imageHoldBack = nil;
	return self;
}

- (void)awakeFromNib
{
	[_pointsTable setDataSource:[_chartData refPointsTableDataSource]];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tableViewSelectionDidChange:) name:NSTableViewSelectionDidChangeNotification object:_pointsTable];
	
	[_zoomPopUp removeAllItems];
	NSMenu * zoomMenu = [_zoomPopUp menu];
	NSMenuItem * item = [[[NSMenuItem alloc] init] autorelease];
	[item setTitle:@"25 %"];
	[item setRepresentedObject:[NSNumber numberWithFloat:.25]];
	[zoomMenu addItem:item];
	item = [[[NSMenuItem alloc] init] autorelease];
	[item setTitle:@"50 %"];
	[item setRepresentedObject:[NSNumber numberWithFloat:.50]];
	[zoomMenu addItem:item];
	item = [[[NSMenuItem alloc] init] autorelease];
	[item setTitle:@"75 %"];
	[item setRepresentedObject:[NSNumber numberWithFloat:.75]];
	[zoomMenu addItem:item];
	item = [[[NSMenuItem alloc] init] autorelease];
	[item setTitle:@"100 %"];
	[item setRepresentedObject:[NSNumber numberWithFloat:1.00]];
	[zoomMenu addItem:item];
	
// We observe all control text modification to catch ones of the tableView 
// (_pointsTable) and make images transparent in view.
	[[NSNotificationCenter defaultCenter] addObserver:self 
		selector:@selector(focusDidChange:) 
		name:NSViewFocusDidChangeNotification object:nil];
		
	[[NSNotificationCenter defaultCenter] addObserver:self 
		selector:@selector(controlTextDidEndEditing:) 
		name:NSControlTextDidEndEditingNotification object:nil];
		
	[[NSNotificationCenter defaultCenter] addObserver:self 
		selector:@selector(controlTextDidBeginEditing:) 
		name:NSControlTextDidBeginEditingNotification object:nil];
}


- (IBAction)zoomPopUpWarpper:(id)sender
{
	if ([sender isKindOfClass:[NSPopUpButton class]])
		return [self setZoom:[[sender selectedItem] representedObject]];
}


- (void)setImageData:(NSData *)data withFilePath:(NSString *)filePath
{
	if (filePath != nil) {
		_usesLensEffect = [[NSUserDefaults standardUserDefaults] imageLensPreference:filePath];
		
		MyDocumentController * docCtrler = [NSDocumentController sharedDocumentController];
		
		int newPref = 0;
		if ([(id)docCtrler userWantsLensEffect] == YES) newPref = LensEffectUsed;
		else newPref = LensEffectNotUsed;

		if (newPref != _usesLensEffect && [(id)docCtrler userChangedPreferencies]) {
			// The user change his preference since last open
			_usesLensEffect = newPref;
			[[NSUserDefaults standardUserDefaults] setImageLensPreference:filePath withLensState:_usesLensEffect];
			[(id)docCtrler resetUserChangedPreferencies];
		}
	}
	[self setImageData:data];
}

- (void)setImageData:(NSData *)data
{

	if (data == nil) {
		NSLog(@"%s:%s:%d:Unable to load image : nil Data", __DATE__, __FILE__, __LINE__);
		NSRunAlertPanel(nil,@"This file cannot be loaded : file broken or format not supported",nil,nil,nil);
		@throw [NSException exceptionWithName:@"ExceptionFormatImageNotSupported" 
			reason:[NSString stringWithFormat:@"%s,%d : The image name can't be loaded, image format not supported",
			rindex (__BASE_FILE__, '/') == NULL ? __BASE_FILE__ : &rindex (__BASE_FILE__, '/')[1],
			__LINE__, __FUNCTION__]
			userInfo:[NSDictionary dictionary]];
	}
	
	NSView * imgView;
	if (_usesLensEffect == LensEffectUsed) {
		NSBundle * appBundle = [NSBundle mainBundle];
		NSString * frameWorkPath = [appBundle privateFrameworksPath];
		frameWorkPath = [frameWorkPath stringByAppendingPathComponent:@"ChartViewCG.framework"];
		NSBundle * ChartViewCG = [NSBundle bundleWithPath:frameWorkPath];
		if ([ChartViewCG load]) {
			id CoreImageViewClass = [ChartViewCG classNamed:@"ChartViewCG"];
			imgView = [[CoreImageViewClass alloc] initWithController:self ];
			[_zoomPopUp selectItemWithTitle:@"50 %"];
			[(id)imgView setImageData:data andMarginsWidth:50.];
			[imgView setToolTip:@"Click to display lens and set its position"];
		} else {
			NSRunCriticalAlertPanel(nil,@"Could not load ChartViewCG bundle",nil,nil,nil);
			[NSApp terminate];
		}
	} else {
		imgView = [[ImageViewWarpper alloc] initWithImageView:[[[ChartViewNSImage alloc] initWithController:self] autorelease] andMarginsWidth:50.];
		_imageHoldBack = [[NSImage alloc] init];
		[_lensMagSlider setEnabled:NO];
		[_zoomPopUp selectItemAtIndex:[_zoomPopUp numberOfItems] - 1];
		[_zoomPopUp setEnabled:NO];
		[(id)_imageHoldBack initWithData:data];
		[(NSImageView *)imgView setImage:_imageHoldBack];
		[_scrollView setDocumentCursor:[NSCursor crosshairCursor]];
	}

	[(id)imgView initViewFrame];

	[[_scrollView contentView] setBackgroundColor:[NSColor lightGrayColor]];
	[_scrollView setDocumentView:imgView];
	[[imgView window] setDelegate:imgView];
	[[_scrollView documentView] scrollPoint:NSMakePoint(-50.,[[_scrollView documentView] frame].size.height)];
}

#pragma mark -
#pragma mark Chart properties accessors

- (void)initAllData
{
	if ([_bsbVer objectValue] == nil)
		[_chartData setBSBVersion:@"2.0"];
	else
		[self setBSBVersion:_bsbVer];
	
	if ([_chartName objectValue] == nil)
		[_chartData setChartName:@"unamed"];
	else
		[self setChartName:_chartName];
	
	if ([_dpi objectValue] == nil) 
		[_chartData setDPI:[NSNumber numberWithInt:300]];
	else
		[self setDPI:_dpi];
	
	if ([_editionDate objectValue] == nil)
		[_chartData setEditDate:[NSDate date]];
	else
		[self setEditDate:_editionDate];
	
	if ([_geoDatum objectValue] == nil) 
		[_chartData setGeodeticDatum:@"Unkown"];
	else
		[self setGeodeticDatum:_geoDatum];
	
	if ([_projectionParameter objectValue] == nil)
		[_chartData setProjectionParameter:[NSNumber numberWithFloat:0]];
	else
		[self setProjectionParameter:_projectionParameter];
	
	if ([_projection objectValue] == nil)
		[_chartData setProjection:@"Unkown"];
	else
		[self setProjection:_projection];
	
	if ([_scale objectValue] == nil)
		[_chartData setScale:[NSNumber numberWithInt:20000]];
	else
		[self setScale:_scale];
	
	if ([_skewAngle objectValue] == nil)
		[_chartData setSkewAngle:[NSNumber numberWithInt:0]];
	else
		[self setSkewAngle:_skewAngle];
	
	if ([_soundingDatum objectValue] == nil)
		[_chartData setSoundingDatum:@"Unknown"];
	else
		[self setSoundingDatum:_soundingDatum];
	
	if ([_units objectValue] == nil)
		[_chartData setUnit:@"Meters"];
	else
		[self setUnit:_units];
	[[[[_scrollView window] windowController] document] updateChangeCount:NSChangeCleared];
}

- (IBAction)setBSBVersion:(id)sender
{
	[_chartData setBSBVersion:[sender objectValue]];
	[[[[_scrollView window] windowController] document] updateChangeCount:NSChangeDone];
}

- (IBAction)setImageHeight:(id)sender
{
	if ([sender isKindOfClass:[NSNumber class]]) {
		[_chartData setImageHeight:sender];
	} else {
		[_chartData setImageHeight:[sender objectValue]];
	}
	[[[[_scrollView window] windowController] document] updateChangeCount:NSChangeDone];
}

- (IBAction)setImageWidth:(id)sender
{
	if ([sender isKindOfClass:[NSNumber class]]) {
		[_chartData setImageWidth:sender];
	} else {
		[_chartData setImageWidth:[sender objectValue]];
	}
	[[[[_scrollView window] windowController] document] updateChangeCount:NSChangeDone];
}

- (IBAction)setChartName:(id)sender
{
	[_chartData setChartName:[sender objectValue]];
	[[[[_scrollView window] windowController] document] updateChangeCount:NSChangeDone];
}

- (IBAction)setDPI:(id)sender
{
	[_chartData setDPI:[sender objectValue]];
	[[[[_scrollView window] windowController] document] updateChangeCount:NSChangeDone];
}

- (IBAction)setEditDate:(id)sender
{
	[_chartData setEditDate:[sender objectValue]];
	[[[[_scrollView window] windowController] document] updateChangeCount:NSChangeDone];
}

- (IBAction)setGeodeticDatum:(id)sender
{
	[_chartData setGeodeticDatum:[sender objectValue]];
	[[[[_scrollView window] windowController] document] updateChangeCount:NSChangeDone];
}

- (IBAction)setLensZoom:(id)sender
{
	if ([[_scrollView documentView] respondsToSelector:@selector(setLensMagnification:)]) {
		[(id)[_scrollView documentView] setLensMagnification:[sender doubleValue]];
	    [_scrollView setNeedsDisplay:YES];
	}
	[[[[_scrollView window] windowController] document] updateChangeCount:NSChangeDone];
}

- (IBAction)setProjection:(id)sender
{
	[_chartData setProjection:[sender objectValue]];
	[[[[_scrollView window] windowController] document] updateChangeCount:NSChangeDone];
}

- (IBAction)setProjectionParameter:(id)sender
{
	[_chartData setProjectionParameter:[sender objectValue]];
	[[[[_scrollView window] windowController] document] updateChangeCount:NSChangeDone];
}

- (IBAction)setScale:(id)sender
{
	[_chartData setScale:[sender objectValue]];
	[[[[_scrollView window] windowController] document] updateChangeCount:NSChangeDone];
}

- (IBAction)setSkewAngle:(id)sender
{
	[_chartData setSkewAngle:[sender objectValue]];
	[[[[_scrollView window] windowController] document] updateChangeCount:NSChangeDone];
}

- (IBAction)setSoundingDatum:(id)sender
{
	[_chartData setSoundingDatum:[sender objectValue]];
	[[[[_scrollView window] windowController] document] updateChangeCount:NSChangeDone];
}

- (IBAction)setUnit:(id)sender
{
	[_chartData setUnit:[sender objectValue]];
	[[[[_scrollView window] windowController] document] updateChangeCount:NSChangeDone];
}

- (IBAction)setZoom:(id)sender
{
	float scale = [sender floatValue];
	[(id)[_scrollView documentView] setImageZoom:scale];
    [_scrollView setNeedsDisplay:YES];
}

- (IBAction)setEditionDate:(id)sender
{
	[_chartData setEditDate:[sender objectValue]];
	[[[[_scrollView window] windowController] document] updateChangeCount:NSChangeDone];
}


#pragma mark -
#pragma mark Events redirected to View

- (void)tableViewSelectionDidChange:(NSNotification *)aNotification
{
	if (_selectionDidChangeWillBeCalledTwice) {
		_selectionDidChangeWillBeCalledTwice = NO;
		return;
	}
	id obj = [aNotification object];
	if ([obj isKindOfClass:[NSTableView class]]){
		int selectedRow = [obj selectedRow];
		if (selectedRow < 0) {
			[[_scrollView documentView] deSelectPoint];
		}  else {
			NSPoint p;
			p.x = [[[obj dataSource] tableView:obj objectValueForTableColumn:[obj tableColumnWithIdentifier:@"x"] row:selectedRow] floatValue];
			p.y = [[[obj dataSource] tableView:obj objectValueForTableColumn:[obj tableColumnWithIdentifier:@"y"] row:selectedRow] floatValue];
			[[_scrollView documentView] selectPoint:p];
		}
	}
}

- (void)focusDidChange:(NSNotification *)aNotification
{
	if ([aNotification object] == _pointsTable) {
		int selectedRow = [_pointsTable selectedRow];
		if (selectedRow < 0) return;
		NSPoint p;
		p.x = [[[_pointsTable dataSource] tableView:_pointsTable objectValueForTableColumn:[_pointsTable tableColumnWithIdentifier:@"x"] row:selectedRow] floatValue];
		p.y = [[[_pointsTable dataSource] tableView:_pointsTable objectValueForTableColumn:[_pointsTable tableColumnWithIdentifier:@"y"] row:selectedRow] floatValue];
		[[_scrollView documentView] setUsesTransparency:YES forPoint:p];
	}
}

- (void)controlTextDidBeginEditing:(NSNotification *)aNotification
{
	if ([aNotification object] == _pointsTable) {
		int editedRow = [_pointsTable editedRow];
		if (editedRow < 0) return;
	
		if ([_pointsTable editedColumn] == [_pointsTable columnWithIdentifier:@"x"]
			|| [_pointsTable editedColumn] == [_pointsTable columnWithIdentifier:@"y"]) {
			_coordsEdited = YES;
			_lastCoords.x = [[[_pointsTable dataSource] tableView:_pointsTable objectValueForTableColumn:[_pointsTable tableColumnWithIdentifier:@"x"] row:editedRow] floatValue];
			_lastCoords.y = [[[_pointsTable dataSource] tableView:_pointsTable objectValueForTableColumn:[_pointsTable tableColumnWithIdentifier:@"y"] row:editedRow] floatValue];
		} else {
			NSPoint p;
			p.x = [[[_pointsTable dataSource] tableView:_pointsTable objectValueForTableColumn:[_pointsTable tableColumnWithIdentifier:@"x"] row:editedRow] floatValue];
			p.y = [[[_pointsTable dataSource] tableView:_pointsTable objectValueForTableColumn:[_pointsTable tableColumnWithIdentifier:@"y"] row:editedRow] floatValue];
			[[_scrollView documentView] setUsesTransparency:YES forPoint:p];
		}
	}
}

- (void)controlTextDidEndEditing:(NSNotification *)aNotification
{
	if ([aNotification object] == _pointsTable) {
		int editedRow = [_pointsTable editedRow];
		if (editedRow < 0) return;
		NSPoint p;
		if (_coordsEdited) {
			memcpy(&p, &_lastCoords, sizeof(NSPoint));
			if ([_pointsTable editedColumn] == [_pointsTable columnWithIdentifier:@"x"]) {
				if (![[NSScanner scannerWithString:[[[aNotification userInfo] objectForKey:@"NSFieldEditor"] string]] scanFloat:&p.x]) {
					NSBeep();
					return;
				}
			} else if ([_pointsTable editedColumn] == [_pointsTable columnWithIdentifier:@"y"]) {
				if (![[NSScanner scannerWithString:[[[aNotification userInfo] objectForKey:@"NSFieldEditor"] string]] scanFloat:&p.y]) {
					NSBeep();
					return;
				}
			}

			if (memcmp(&p,&_lastCoords,sizeof(NSPoint)) == 0) return;
			
			if (_coordsEdited) {
				_coordsEdited = NO;
				[[_scrollView documentView] translatePoint:_lastCoords withDx:(p.x - _lastCoords.x) withdy:(p.y - _lastCoords.y)];
			}
			[self movePointFrom:_lastCoords to:p];
		} else {
			p.x = [[[_pointsTable dataSource] tableView:_pointsTable objectValueForTableColumn:[_pointsTable tableColumnWithIdentifier:@"x"] row:editedRow] floatValue];
			p.y = [[[_pointsTable dataSource] tableView:_pointsTable objectValueForTableColumn:[_pointsTable tableColumnWithIdentifier:@"y"] row:editedRow] floatValue];
			[[_scrollView documentView] setUsesTransparency:NO forPoint:p];
		}
//		[[_scrollView documentView] selectPoint:p];
		[[[[_scrollView window] windowController] document] updateChangeCount:NSChangeDone];
	}
}

#pragma mark -
#pragma mark Points management

// Add an unknown point
- (void)pointEvent:(NSPoint)p
{
	id dataSource = [_pointsTable dataSource];
	int row = [dataSource numberOfRowsInTableView:_pointsTable]; 
	[dataSource tableView:_pointsTable 
		setObjectValue:[NSNumber numberWithFloat:p.x]
		forTableColumn:[_pointsTable tableColumnWithIdentifier:@"x"]
		row:row];
	[dataSource tableView:_pointsTable 
		setObjectValue:[NSNumber numberWithFloat:p.y]
		forTableColumn:[_pointsTable tableColumnWithIdentifier:@"y"]
		row:row];
	[_pointsTable reloadData];
	[self selectPoint:p];	
	[[[[_scrollView window] windowController] document] updateChangeCount:NSChangeDone];
	[_pointsTable editColumn:[_pointsTable columnWithIdentifier:@"lat"] 
		row:[[_pointsTable dataSource] numberOfRowsInTableView:_pointsTable] - 1
		withEvent:nil select:YES];
}

// Select a point
- (void)selectPoint:(NSPoint)p
{
	int nbPoints = [[_pointsTable dataSource] numberOfRowsInTableView:_pointsTable];
	int i = 0;
	BOOL notFound = YES;
	while (i < nbPoints && notFound) {
		float currX = [[[_pointsTable dataSource] tableView:_pointsTable objectValueForTableColumn:[_pointsTable tableColumnWithIdentifier:@"x"] row:i] floatValue];
		float currY;
		if (p.x == currX) {
			currY = [[[_pointsTable dataSource] tableView:_pointsTable objectValueForTableColumn:[_pointsTable tableColumnWithIdentifier:@"y"] row:i] floatValue];
			if (p.y == currY) {
				notFound = NO;
				if ([_pointsTable selectedRow] == i) {
					// Order is important here
					_selectionDidChangeWillBeCalledTwice = YES;
					[_pointsTable deselectRow:i];
				}
				[_pointsTable selectRowIndexes:[NSIndexSet indexSetWithIndex:i] byExtendingSelection:NO];
			}
		}
		i++;
	} 
}

// Delete a point

- (void)deletePoint:(NSPoint)p
{
	int nbPoints = [[_pointsTable dataSource] numberOfRowsInTableView:_pointsTable];
	int i = 0;
	BOOL notFound = YES;
	while (i < nbPoints && notFound) {
		NSPoint curr;
		curr.x = [[[_pointsTable dataSource] tableView:_pointsTable objectValueForTableColumn:[_pointsTable tableColumnWithIdentifier:@"x"] row:i] floatValue];
		if (p.x == curr.x) {
			curr.y = [[[_pointsTable dataSource] tableView:_pointsTable objectValueForTableColumn:[_pointsTable tableColumnWithIdentifier:@"y"] row:i] floatValue];
			if (p.y == curr.y) {
				notFound = NO;
				[[_pointsTable dataSource] tableView:_pointsTable setObjectValue:nil forTableColumn:nil row:i];
				[_pointsTable reloadData];
				[_pointsTable selectRowIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0,0)] byExtendingSelection:NO];
			}
		}
		i++;
	}
	[[[[_scrollView window] windowController] document] updateChangeCount:NSChangeDone];
}

- (void)movePointFrom:(NSPoint)dep to:(NSPoint)dest
{
	int nbPoints = [[_pointsTable dataSource] numberOfRowsInTableView:_pointsTable];
	int i = 0;
	BOOL notFound = YES;
	while (i < nbPoints && notFound) {
		NSPoint curr;
		curr.x = [[[_pointsTable dataSource] tableView:_pointsTable objectValueForTableColumn:[_pointsTable tableColumnWithIdentifier:@"x"] row:i] floatValue];
		if (dep.x == curr.x) {
			curr.y = [[[_pointsTable dataSource] tableView:_pointsTable objectValueForTableColumn:[_pointsTable tableColumnWithIdentifier:@"y"] row:i] floatValue];
			if (dep.y == curr.y) {
				notFound = NO;
				[[_pointsTable dataSource] tableView:_pointsTable setObjectValue:[NSNumber numberWithInt:(int)dest.x] forTableColumn:[_pointsTable tableColumnWithIdentifier:@"x"] row:i];
				[[_pointsTable dataSource] tableView:_pointsTable setObjectValue:[NSNumber numberWithInt:(int)dest.y] forTableColumn:[_pointsTable tableColumnWithIdentifier:@"y"] row:i];
				[_pointsTable reloadData];
			}
		}
		i++;
	}
	[[[[_scrollView window] windowController] document] updateChangeCount:NSChangeDone];
}

#pragma mark -
#pragma mark Accessors

- (ChartData *)chartData;
{
	NSSize chartSize = [[_scrollView documentView] imageSize];
	[self setImageWidth:[NSNumber numberWithInt:(int)roundf(chartSize.width)]];
	[self setImageHeight:[NSNumber numberWithInt:(int)roundf(chartSize.height)]];
	return _chartData;
}

- (NSView *)imageView;
{
	return [_scrollView documentView];
}

#pragma mark -
#pragma mark Destructor

- (void) dealloc {
	[_chartData release];
	if (_imageHoldBack) {
		int i = [_imageHoldBack retainCount];
		for (;i > 0;i--)
			[_imageHoldBack release];
	}
	if (![[_scrollView documentView] isKindOfClass:[NSView class]]) 
		[[_scrollView documentView] release];
		
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	[super dealloc];
}

@end

#pragma mark -
#pragma mark User display preferencies

@implementation NSUserDefaults(ImageEffectsPreferences)
- (void)setImageLensPreference:(NSString *)filepath withLensState:(int)l
{
    NSMutableDictionary *theDictionnary = nil;
    NSData *theData = [self dataForKey:@"ImagesList"];
	
    if (theData != nil) {
        theDictionnary = [(NSDictionary *)[NSUnarchiver unarchiveObjectWithData:theData] mutableCopy];
	} else {
		theDictionnary = [NSMutableDictionary dictionary];
	}
	
	[theDictionnary setValue:[NSNumber numberWithInt:l] forKey:filepath];
    theData=[NSArchiver archivedDataWithRootObject:theDictionnary];
    [self setObject:theData forKey:@"ImagesList"];
}

- (int)imageLensPreference:(NSString *)filepath
{
    NSDictionary *theDictionnary = nil;
    NSData *theData = [self dataForKey:@"ImagesList"];
	
    if (theData != nil) {
        theDictionnary = (NSDictionary *)[NSUnarchiver unarchiveObjectWithData:theData];
	} else {
		NSData *theData=[NSArchiver archivedDataWithRootObject:[NSDictionary dictionary]];
	    [self setObject:theData forKey:@"ImagesList"];
		return LensEffectUnknown;
	}
		
	id obj = [theDictionnary objectForKey:filepath];
	if (obj == nil) return LensEffectUnknown; 
    return [obj intValue];
}


@end

