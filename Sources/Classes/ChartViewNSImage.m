
//  Created by Nicolas Cherel.
//  Copyright 2005 Magic Instinct Software. 
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


#import "ChartViewNSImage.h"

static inline BOOL isPointInCircle(NSPoint p, NSPoint center, float radius) {
	NSPoint distFromCenter;
	distFromCenter.x = (center.x - p.x);
	distFromCenter.y = (center.y - p.y);
	return sqrtf(powf(distFromCenter.x, 2) + powf(distFromCenter.y, 2)) < radius;
}

static inline BOOL isPointInRect(NSPoint p, NSRect r) {
	return (p.x > r.origin.x 
		&& p.x - r.origin.x < r.size.width 	
		&& p.y > r.origin.y
		&& p.y - r.origin.y < r.size.height);
}	

@interface NoRepondingNSImageView : NSImageView
@end

@implementation NoRepondingNSImageView
- (void)mouseDown:(NSEvent *)theEvent { [[self superview] mouseDown:theEvent];}
- (void)mouseUp:(NSEvent *)theEvent { [[self superview] mouseUp:theEvent];}

@end

double DRAG_DELAY = 0.05;

@implementation ChartViewNSImage

- (id)initWithController:(ChartDocumentController *)controller
{
	[super init];
	_controller = controller;
	
	/** image inits **/
	
	NSImageRep * tmp =[[NSImageRep imageRepWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"flag" ofType:@"png"]] retain];
	
	FLAG = [[NSImage alloc] init];
	[FLAG addRepresentation:tmp];
	
	tmp =[[NSImageRep imageRepWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"flag-transparent" ofType:@"png"]] retain];
	
	FLAG_TRANSPARENT = [[NSImage alloc] init];
	[FLAG_TRANSPARENT addRepresentation:tmp];
	
	NSScanner * scanFlagCenter = [NSScanner scannerWithString:[NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"flag-center" ofType:@"txt"]]];
	[scanFlagCenter scanFloat:&FLAG_CENTER.x];
	[scanFlagCenter scanString:@"," intoString:nil];
	[scanFlagCenter scanFloat:&FLAG_CENTER.y];
	FLAG_CENTER.y = fabsf(FLAG_CENTER.y - [FLAG size].height);
	
	tmp =[[NSImageRep imageRepWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"flag-select" ofType:@"png"]] retain];
	
	NSImage * tmpImg = [[NSImage alloc] init];
	[tmpImg addRepresentation:tmp];
	FLAG_SELECT = [[NoRepondingNSImageView alloc] init];
	[FLAG_SELECT setImage:tmpImg];
	[FLAG_SELECT setFrameSize:[tmpImg size]];
	scanFlagCenter = [NSScanner scannerWithString:[NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"flag-select-center" ofType:@"txt"]]];
	[scanFlagCenter scanFloat:&FLAG_SELECT_CENTER.x];
	[scanFlagCenter scanString:@"," intoString:nil];
	[scanFlagCenter scanFloat:&FLAG_SELECT_CENTER.y];
	FLAG_SELECT_CENTER.y = fabsf(FLAG_SELECT_CENTER.y - [[FLAG_SELECT image] size].height);

	NSImage	*BUTTON_REMOVE;
	NSImage	*BUTTON_REMOVE_UP;
	NSImage	*BUTTON_REMOVE_DOWN;

	tmp =[[NSImageRep imageRepWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"button-remove-normal" ofType:@"png"]] retain];
	
	BUTTON_REMOVE = [[NSImage alloc] init];
	[BUTTON_REMOVE addRepresentation:tmp];

	tmp =[[NSImageRep imageRepWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"button-remove-up" ofType:@"png"]] retain];
	
	BUTTON_REMOVE_UP = [[NSImage alloc] init];
	[BUTTON_REMOVE_UP addRepresentation:tmp];
	
	tmp =[[NSImageRep imageRepWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"button-remove-down" ofType:@"png"]] retain];
	
	BUTTON_REMOVE_DOWN = [[NSImage alloc] init];
	[BUTTON_REMOVE_DOWN addRepresentation:tmp];
	
	_button_remove = [[Button buttonWithImages:[NSDictionary dictionaryWithObjectsAndKeys:
						BUTTON_REMOVE,@"normal",
						BUTTON_REMOVE_UP,@"up",
						BUTTON_REMOVE_DOWN,@"down",nil]
					withAction:@selector(performButtonDelete)
					onObject:self] retain];
	[_button_remove setCurrentImage:@"normal"];
	_button_view = [[NoRepondingNSImageView alloc] init];
	[_button_view setImage:[_button_remove image]];
	[_button_view setFrameSize:[[_button_remove image] size]];
	
	NSSize imgSize = [[self image] size];
	[_controller setImageWidth:[NSNumber numberWithInt:imgSize.width]];
	[_controller setImageHeight:[NSNumber numberWithInt:imgSize.height]];
	_scale = 1;
	return self;
}

#pragma mark -
#pragma mark Events handling
/*
- (void)mouseEntered:(NSEvent *)theEvent
{
	[[NSCursor crosshairCursor] push];
}

- (void)mouseExited:(NSEvent *)theEvent
{
	[[NSCursor arrowCursor] set];
}
*/
- (void)mouseDown:(NSEvent *)theEvent
{
	[[self window] makeFirstResponder:self];
	
	NSPoint mouseLoc = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	mouseLoc.x = mouseLoc.x * _scale;
	mouseLoc.y = mouseLoc.y * _scale;
	NSRect oldFrame = [_button_view frame];
	if (isPointInRect(mouseLoc, oldFrame)) {
		[_button_remove setCurrentImage:@"down"];
		[_button_view setImage:[_button_remove image]];
		[_button_view setFrame:oldFrame];
		[self setNeedsDisplayInRect:[_button_view frame]];
	}

	[_flag_select removeFromSuperview];
	[self setNeedsDisplayInRect:[_flag_select frame]];
}

- (void)mouseUp:(NSEvent *)theEvent
{
// Select and perform action corresponding to the mouse location (selection, 
// create point, update display)
	BOOL canCreatePoint = YES; // To avoid point creation after a drag
	if (_drag) {
		_drag = NO;
		_dragged_obj = nil;
		if(![_clock_at_drag_begin timeIntervalSinceNow] < -DRAG_DELAY)
			return;
		else
			canCreatePoint = NO;
	}
	NSPoint mouseLoc = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	mouseLoc.x = mouseLoc.x * _scale;
	mouseLoc.y = mouseLoc.y * _scale;	
	if (_flag_selected) {
		BOOL mouseWasOnButton = NO;
		NSRect oldFrame = [_button_view frame];
		if ( isPointInRect(mouseLoc, oldFrame)) {
			[_button_remove setCurrentImage:@"up"];
			[_button_view setImage:[_button_remove image]];
			[_button_remove performAction];
			mouseWasOnButton = YES;
		} else {
			[_button_remove setCurrentImage:@"normal"];
			[_button_view setImage:[_button_remove image]];
		}
		[_button_view setFrame:oldFrame];
		[self setNeedsDisplayInRect:[_button_view frame]];
		if (mouseWasOnButton) return;
	}

	
	BOOL selectionChanged = NO; // to avoid point creation if selection changed.
	NSEnumerator * enu = [[self subviews] objectEnumerator];
	NSImageView * obj;
	[_flag_select removeFromSuperview];
	while (obj = [enu nextObject]) {
		NSPoint center = [obj frame].origin;
		center.x += FLAG_CENTER.x;
		center.y += FLAG_CENTER.y;
		if (obj != _flag_select && obj != _flag_selected && isPointInCircle(mouseLoc,center,FLAG_CENTER.x)) {
			selectionChanged = YES;
			center.y = roundf (fabsf(center.y - [self frame].size.height));
			[_controller selectPoint:center];
			break;
		}
	}

	if (!selectionChanged && canCreatePoint) {
		NSPoint plotPosition = [self convertPoint:[theEvent locationInWindow] fromView:nil];
		plotPosition.x -= FLAG_CENTER.x;
		plotPosition.y -= FLAG_CENTER.y;
		
		NSImageView * flag = [[NoRepondingNSImageView alloc]init] ;
		[flag setImage:FLAG];
		[flag setFrameOrigin:plotPosition];
		[flag setFrameSize:[FLAG size]];
		[flag setNextResponder:self];
		
		[self addSubview:flag];
		[self setNeedsDisplayInRect:[flag frame]];
		
		NSPoint newPoint = [self convertPoint:[theEvent locationInWindow] fromView:nil];
		newPoint.y = roundf (fabsf(newPoint.y - [self frame].size.height));
		[_controller pointEvent:newPoint];
	}
}

- (void)mouseMoved:(NSEvent *)theEvent
{
// update selection display
	NSPoint mouseLoc = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	NSEnumerator * enu = [[self subviews] objectEnumerator];
	NSImageView * obj;
	[_flag_select removeFromSuperview];
	while (obj = [enu nextObject]) {
		NSPoint center = [obj frame].origin;
		center.x += FLAG_CENTER.x;
		center.y += FLAG_CENTER.y;
		if (obj != _flag_select && obj != _flag_selected && obj != _button_view && isPointInCircle(mouseLoc,center,FLAG_CENTER.x)) {
			_flag_select = FLAG_SELECT;
			[_flag_select setFrameOrigin:NSMakePoint(center.x - FLAG_SELECT_CENTER.x,
				center.y - FLAG_SELECT_CENTER.y)];
			[self addSubview:_flag_select positioned:NSWindowBelow relativeTo:self];
			[self setNeedsDisplayInRect:[_flag_select frame]];
		} else {
			if (_flag_selected) {
				NSRect oldFrame = [_button_view frame];
				if (obj == _button_view && isPointInRect(mouseLoc, oldFrame)) {
					[_button_remove setCurrentImage:@"up"];
					[_button_view setImage:[_button_remove image]];
				} else if (obj == _button_view) {
					[_button_remove setCurrentImage:@"normal"];
					[_button_view setImage:[_button_remove image]];
				}
				[_button_view setFrame:oldFrame];
				[self setNeedsDisplayInRect:oldFrame];
			}
		}
	}
}

- (void)mouseDragged:(NSEvent *)theEvent
{
	NSPoint mouseLoc = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	NSEnumerator * enu = [[self subviews] objectEnumerator];
	NSImageView * obj;
	[_flag_select removeFromSuperview];
	while (obj = [enu nextObject]) {
		NSPoint center = [obj frame].origin;
		center.x += FLAG_CENTER.x;
		center.y += FLAG_CENTER.y;
		if (obj != _flag_select && obj != _flag_selected && isPointInCircle(mouseLoc,center,FLAG_CENTER.x)) {
			_drag = YES;
			
			if (_dragged_obj == nil) {
				[_clock_at_drag_begin release];
				_clock_at_drag_begin = [[NSDate date] retain];
				_dragged_obj = obj;
			}
			break;
		}
	}

	if (_drag && [_clock_at_drag_begin timeIntervalSinceNow] < -DRAG_DELAY) {
		[_flag_selected removeFromSuperview];
		[_flag_selected autorelease];
		_flag_selected = nil;
		
		if ([NSCursor currentCursor] != [NSCursor closedHandCursor]) 
			[[NSCursor closedHandCursor] set];
		[self autoscroll:theEvent];
		[self setNeedsDisplayInRect:[_dragged_obj frame]];
		
		NSPoint origLoc = [_dragged_obj frame].origin;
		NSPoint newLoc = origLoc;
		newLoc.x += [theEvent deltaX];
		newLoc.y -= [theEvent deltaY];
		NSPoint mouseLoc = [self convertPoint:[theEvent locationInWindow] fromView:nil];
		NSRect visibleArea = [self visibleRect];
		if (mouseLoc.x <= visibleArea.origin.x || mouseLoc.x >= visibleArea.size.width + visibleArea.origin.x) 
			newLoc.x = mouseLoc.x - FLAG_CENTER.x + _deltaDragFlagX;
		else 
			_deltaDragFlagX = newLoc.x + FLAG_CENTER.x - mouseLoc.x;
			
		if (mouseLoc.y <= visibleArea.origin.y || mouseLoc.y >= visibleArea.size.height + visibleArea.origin.y)
			newLoc.y = mouseLoc.y - FLAG_CENTER.y + _deltaDragFlagY;
		else 
			_deltaDragFlagY = newLoc.y + FLAG_CENTER.y - mouseLoc.y;

		[_dragged_obj setFrameOrigin:newLoc];
		
		[_button_view removeFromSuperview];
		
		origLoc.x += FLAG_CENTER.x;
		origLoc.y += FLAG_CENTER.y;
		origLoc.y = roundf (fabsf (origLoc.y - [self frame].size.height));
		newLoc.x += FLAG_CENTER.x;
		newLoc.y += FLAG_CENTER.y;
		newLoc.y = roundf (fabsf (newLoc.y - [self frame].size.height));
		[_controller movePointFrom:origLoc to:newLoc];
		[self setNeedsDisplayInRect:[_dragged_obj frame]];
	}
}

- (void)performButtonDelete
{
	if (_flag_selected) {
		NSPoint p = [_flag_selected frame].origin;
		p.x += FLAG_SELECT_CENTER.x;
		p.y += FLAG_SELECT_CENTER.y;
		p.y = roundf (p.y);
		NSEnumerator * enu = [[self subviews] objectEnumerator];
		NSImageView * obj;
		while (obj = [enu nextObject]) {
			NSPoint center = [obj frame].origin;
			center.x += FLAG_CENTER.x;
			center.y += FLAG_CENTER.y;
			center.y = roundf(center.y);
			if (obj != _flag_select && obj != _flag_selected 
				&& center.x == p.x && center.y == p.y) {
				[obj removeFromSuperview];
				break;
			}
		}
		
		p.y = roundf (- (p.y - [self frame].size.height));
		[_controller deletePoint:p];
		[_flag_selected removeFromSuperview];
		[_flag_selected autorelease];
		_flag_selected = nil;
		[_button_view removeFromSuperview];

		[self setNeedsDisplay:YES];
	}

}

- (void)viewWillMoveToWindow:(NSWindow *)newWindow
{
	[newWindow makeFirstResponder:self];
	[newWindow setAcceptsMouseMovedEvents:YES];
	[self removeTrackingRect:_trackTag];
	
	_trackTag = [self addTrackingRect:[self visibleRect] owner:self userData:nil assumeInside:NO];
}

- (void)setImageZoom:(double)scale
{
	NSAffineTransform * scaler = [NSAffineTransform transform];
	[scaler scaleBy:scale/_scale];
	NSEnumerator * enu = [[self subviews] objectEnumerator];
	id  obj;
	while (obj = [enu nextObject]) {
		NSPoint origBufCalc = [obj frame].origin;
		origBufCalc.x += FLAG_CENTER.x;
		origBufCalc.y += FLAG_CENTER.y;
		origBufCalc = [scaler transformPoint:origBufCalc];
		origBufCalc.x -= FLAG_CENTER.x;
		origBufCalc.y -= FLAG_CENTER.y;		
		[obj setFrameOrigin:origBufCalc];
	}
	_scale = scale;
}

- (void)selectPoint:(NSPoint)p
{	
	if (_flag_selected) {
		[_flag_selected autorelease];
		[_flag_selected removeFromSuperview];
	}
	_flag_selected = [[NoRepondingNSImageView alloc] init];
	[_flag_selected setImage:[FLAG_SELECT image]];
	[_flag_selected setFrame:[FLAG_SELECT frame]];
	[_flag_selected setFrameOrigin:NSMakePoint(p.x - FLAG_SELECT_CENTER.x, 
		- (p.y + FLAG_SELECT_CENTER.y - [self frame].size.height))];
	[self addSubview:_flag_selected positioned:NSWindowBelow relativeTo:self];
	
	// "remove button" positioning
	NSAffineTransform * at = [NSAffineTransform transform];
	[at translateXBy:[_flag_selected frame].size.width yBy:0];
	[_button_view setFrameOrigin:[at transformPoint:[_flag_selected frame].origin]];
	[self addSubview:_button_view];

	p.y = -(p.y - [self frame].size.height);
	
	if (!isPointInRect(p,[self visibleRect])) {
		p.x -= [self visibleRect].size.width / 2;
		p.y -= [self visibleRect].size.height / 2;
		[self scrollPoint:p];
	}
	[self setNeedsDisplay:YES];
}

- (void)deSelectPoint
{
	if (_flag_selected) {
		[_flag_selected removeFromSuperview];
		_flag_selected = nil;
	}
	[self setNeedsDisplay:YES];
}

- (void)keyDown:(NSEvent *)theEvent
{ 
	int key = [[theEvent characters] characterAtIndex:0];
	if (key == NSDeleteFunctionKey || key == '\177') {
		[self performButtonDelete];
	}
	[self setNeedsDisplay:YES];
}

- (void)setUsesTransparency:(BOOL)b forPoint:(NSPoint)p
{
	NSEnumerator * enu = [[self subviews] objectEnumerator];
	NSImageView * old;
	while (old = [enu nextObject]) {
		NSPoint center = [old frame].origin;
		center.x += FLAG_CENTER.x;
		center.y += FLAG_CENTER.y;
		center.y = - roundf(center.y -  [self frame].size.height);
		if (old != _flag_select && old != _flag_selected 
			&& center.x == p.x && center.y == p.y) {
			
			NSImageView * flag = [[NoRepondingNSImageView alloc]init] ;
			if (b) {
				[flag setImage:FLAG_TRANSPARENT];
				[flag setFrameSize:[FLAG_TRANSPARENT size]];
			} else {
				[flag setImage:FLAG];
				[flag setFrameSize:[FLAG size]];
			}
			[flag setFrameOrigin:[old frame].origin];
			[old removeFromSuperview];
			[flag setNextResponder:self];
			[self addSubview:flag];
			[self setNeedsDisplay:YES];
			break;
		}
	}
}

#pragma mark -
#pragma mark Points management

- (void)translatePoint:(NSPoint)p withDx:(float)x withdy:(float)y
{
	NSEnumerator * enu = [[self subviews] objectEnumerator];
	NSImageView * old;
	while (old = [enu nextObject]) {
		NSPoint center = [old frame].origin;
		NSPoint orig = center;
		center.x += FLAG_CENTER.x;
		center.y += FLAG_CENTER.y;
		center.y = - roundf(center.y -  [self frame].size.height);
		if (old != _flag_select && old != _flag_selected 
			&& center.x == p.x && center.y == p.y) {
			orig.x += x;
			orig.y -= y;
			[old setFrameOrigin:orig];
		}
	}
}

#pragma mark -
#pragma mark Drawing method

- (void)setNeedsDisplay:(BOOL)b
{
	[super setNeedsDisplay:b];
	[[self superview] setNeedsDisplay:b];
}

- (void)setNeedsDisplay
{
	[self setNeedsDisplay:YES];
}

- (void)drawRect:(NSRect)aRect
{
	
	[super drawRect:aRect];
	[self removeTrackingRect:_trackTag];
	_trackTag = [self addTrackingRect:[self visibleRect] owner:self userData:nil assumeInside:NO];
}

#pragma mark -
- (void) dealloc {
	[[self subviews] makeObjectsPerformSelector:@selector(release)];
	[super dealloc];
}

@end
