//
//  OpenPanelAccessory.m
//  RasterChart2BSB
//
//  Created by Nicolas Cherel on 18/07/05.
//  Copyright 2005 Magic Instinct Software.  
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//

#import "OpenPanelAccessory.h"


@implementation OpenPanelAccessory

#pragma mark -
#pragma mark User preferencies modification observation 


- (void)clickControl
{
	_prefChanged = YES;
}

- (void)resetPrefChanged
{
	_prefChanged = NO;
}

- (BOOL)prefChanged
{
	return _prefChanged;
}

- (void)setPrefChanged;
{
	[self clickControl];
}

#pragma mark -

- (id) init {
	self = [super init];
	if (self != nil) {
		_checkBox = [[NSButton alloc] init];
		[_checkBox setButtonType:NSSwitchButton];
		[_checkBox setTitle:@"Use special effects (Lens display - disable for huge images)"];
		[_checkBox setTarget:self];
		[_checkBox setAction:@selector(clickControl)];
		[_checkBox setFrameSize:[[_checkBox cell] cellSize]];
		[self setFrame:[_checkBox frame]];
		[self addSubview:_checkBox];
		SInt32 MacVersion;
		if (Gestalt(gestaltSystemVersion, &MacVersion) == noErr && MacVersion < 0x1040)
			[_checkBox setEnabled:NO];
		[_checkBox release]; // Retained in addSubview
		
	}
	_prefChanged = NO;	
	return self;
}
- (BOOL) lensEffectSelected;
{
	return [_checkBox state] == NSOnState;
}

- (void) dealloc {
	[super dealloc];
}

@end
