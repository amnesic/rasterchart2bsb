//
//  ImageViewWarpper.m
//  RasterChart2BSB
//
//  Created by Nicolas Cherel on 05/07/05.
//  Copyright 2005 Magic Instinct Software.  
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//

#import "ImageViewWarpper.h"


@implementation ImageViewWarpper
- (ImageViewWarpper *) initWithImageView:(NSImageView *)imageView andMarginsWidth:(float)width
{
	[super init];
	_marginsWidth = width;
	_imageView = [imageView retain];
	[self addSubview:_imageView];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(framedDidChanged) name:NSViewFrameDidChangeNotification object:[self superview]];
	return self;
}

- (void)framedDidChanged
{
	[self setFrame:[_imageView frame]];
}

- (void)initViewFrame
{
	[self setImageZoom:1.];
}

- (void)setFrame:(NSRect) r
{
// avoids blank View 	
	r.origin = NSMakePoint(0,0);
	
	// avoids recursive loop
	if (memcmp (&_lastSetViewFrame, &r, sizeof(NSRect)) != 0) {
		_lastSetViewFrame = r;
		[_imageView setFrame:r];
	}

	NSRect cont = [[self superview] frame];

	r.size.width += _marginsWidth * 2;
	if (r.size.width < cont.size.width) {
		r.origin.x = - (_marginsWidth + (cont.size.width - r.size.width) / 2);
		r.size.width = cont.size.width;
	} else {
		r.origin.x -= _marginsWidth;
	}
	
	r.size.height += _marginsWidth * 2;
	if (r.size.height < cont.size.height) {
		r.origin.y = - (_marginsWidth + (cont.size.height - r.size.height) / 2);
		r.size.height = cont.size.height;
	} else {
		r.origin.y -= _marginsWidth;
	}
	
	// avoids recursive loop
	if (memcmp (&_lastSetFrame, &r, sizeof(NSRect)) != 0) {
		_lastSetFrame = r;
		[super setFrame:r];
		[super setBoundsOrigin:r.origin];
	}

}

// To avoid Loop in responder Chain, whe hide mouseMoved (keyDown)
// Warning !! this code works only is mouseMoved is redirected
// it works because we use MouseMovedTransmitter as window nextResponder
- (void)mouseMoved:(NSEvent *)theEvent
{
	[_imageView mouseMoved:theEvent];
}

- (void)keyDown:(NSEvent *)theEvent
{
	[_imageView keyDown:theEvent];
}

// End Warning

- (void)setImage:(NSImage *)image
{
	[_imageView setImage:image];
}


- (NSSize)imageSize
{
	return [[_imageView image] size];
}

- (void)setImageZoom:(double)scale
{
    NSAffineTransform *at = [NSAffineTransform transform];
    [at scaleBy:scale];
	NSRect imgFrame;
	imgFrame.size = [at transformSize:[[_imageView image] size]];
    [self setFrame:imgFrame];
	[(id) _imageView setImageZoom:scale];
}

/* forwarding does not work :(
- (void)forwardInvocation:(NSInvocation *)anInvocation 
{ // FORWARD FOR NSIMAGEVIEW METHODS (W'RE WARPPING NSIMAGEVIEW)
	NSLog (@"%s(%@)", __FUNCTION__, [anInvocation description]);
	if ([_imageView respondsToSelector:[anInvocation selector]]) 
		[anInvocation invokeWithTarget: _imageView]; 
	else
		[super forwardInvocation:anInvocation]; 
} 
*/

- (void)selectPoint:(NSPoint)p
{
	(void) [(id)_imageView selectPoint:p];
}

- (void)deSelectPoint
{
	(void) [(id)_imageView deSelectPoint];
}

- (void)setUsesTransparency:(BOOL)b forPoint:(NSPoint)p
{
	(void) [(id)_imageView setUsesTransparency:(BOOL)b forPoint:(NSPoint)p];
}

- (void)translatePoint:(NSPoint)p withDx:(float)x withdy:(float)y
{
	(void) [(id)_imageView translatePoint:p withDx:x withdy:y];
}
#pragma mark -
- (void) dealloc {
	[_imageView release];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[super dealloc];
}

@end
