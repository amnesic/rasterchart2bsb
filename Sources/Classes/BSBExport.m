//
//  BSBExport.m
//  RasterChart2BSB
//
//  Created by Nicolas Cherel on 02/08/05.
//  Copyright 2005 Magic Instinct Software.  
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//

#import "BSBExport.h"
#include "magick_access.h"


NSString * temporaryDirectory(void)
{
	NSFileManager * filemngr = [NSFileManager defaultManager];
	BOOL isDirectory;
		
	NSMutableString * tempPath = [NSMutableString stringWithString:NSTemporaryDirectory()];
	[tempPath appendString:@"/"];
	[tempPath appendString:[[NSBundle mainBundle] bundleIdentifier]];
	
	if ([filemngr fileExistsAtPath:tempPath isDirectory:&isDirectory]  && !isDirectory) {
		[tempPath appendString:@".1"];
	}
	if (![filemngr fileExistsAtPath:tempPath isDirectory:&isDirectory]) {
		[filemngr createDirectoryAtPath:tempPath attributes:nil];
	}
	
	[tempPath appendString:@"/"];
	[tempPath appendString:[NSString stringWithFormat:@"%d", getpid()]];
	if (![filemngr fileExistsAtPath:tempPath isDirectory:&isDirectory]) {
		[filemngr createDirectoryAtPath:tempPath attributes:nil];
	}	

	return tempPath;
}

@implementation BSBExport

- (id)initWithObserver:(id)anObserver andSelector:(SEL)aSelector;
{
	[super init];
	if (anObserver != nil) {
		_target = anObserver;
		_selector = aSelector;
	} 
	return self;
}

int my_monitor (const char *text,  long long offset, unsigned long long span, void * clientData) {
	id target = *((id *) ((void **) clientData)[0]);
	SEL selector = *((SEL *) ((void **) clientData)[1]);
	BOOL usesDithering = *((BOOL *) ((void **) clientData)[2]);
	if (clientData) {
		double percent = 0;
		if (0 == strcmp(text, "Load/Image")) {
			percent = ((float) offset / (float) span) / (usesDithering ? 6 : 5);
			text = "Loading into memory";
		} else if (0 == strcmp(text, "Classify/Image")) {
			percent = (((float) offset / (float) span) + 1) / (usesDithering ? 6 : 5);
			text = "Analyzing colors";
		} else if (0 == strcmp(text, "Reduce/Image")) {
			percent = (((float) offset / (float) span) + 2) / (usesDithering ? 6 : 5);
			text = "Reducing to 128 colors";
		} else if (0 == strcmp(text, "Assign/Image")) {
			percent = (((float) offset / (float) span) + 3) / (usesDithering ? 6 : 5);
			text = "Reducing to 128 colors";
		} else if (0 == strcmp(text, "Dither/Image")) {
			percent = (((float) offset / (float) span) + 4) / (usesDithering ? 6 : 5);
			text = "Dithering";
		} else if (0 == strcmp(text, "Save/Image")) {
			percent = (((float) offset / (float) span) + (usesDithering ? 5 : 4)) / (usesDithering ? 6 : 5);
			text = "Finishing";
		}

		NSDictionary * param = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithCString:text],@"text",
			[NSNumber numberWithDouble:(double)percent],@"value",nil];
		[target performSelector:selector withObject:param];
	}
	return monitorReturnYes();
}




- (NSData *)exportReferencedImage:(NSData *)anImage withGeoData:(ChartData *)someData
{	
	NSString * tempPath = temporaryDirectory();
	unsigned char * data = NULL;
	size_t dataLength;
	NSString * tiffFilePath = [NSString stringWithFormat:@"%@/%@",tempPath,@"inputTiff.tiff"];
	NSString * kapTemplatePath = [NSString stringWithFormat:@"%@/%@",tempPath,@"template.kap"]; 
	NSString * bsbResult = [NSString stringWithFormat:@"%@/%@",tempPath,@"bsbOut.kap"]; 
	NSMutableString * kapTemplate = [NSMutableString string];

	// Convertion to tiff 128 colors
	[[[NSBitmapImageRep imageRepWithData:anImage] TIFFRepresentation] writeToFile:tiffFilePath atomically:NO];
	void * monitor_user_data[3] = {&_target, &_selector, &_usesDithering};
	int magickResult = convertFileIntoTIFF128Colors([tiffFilePath cString], my_monitor, monitor_user_data ,_usesDithering, &data, &dataLength);

	
	switch (magickResult) {
		case 0 : {
			break;
		}
		case MAGICK_CANT_MAKE_AN_INDEXED_IMAGE : {
			NSRunCriticalAlertPanel(@"Impossible to create a colored tiff image",@"To create a BSB file, we need to generate a temporary colormaped (eg. indexed palette) TIFF file. This cannot be done here. This error occurs for example with B&W image : so before using RasterChart2BSB, modify your original image with some graphic tool such as GraphicConverter?, changing it to 32768 colors and adding a color pixel over the image with the pen.",@"Ok, i'll do it",nil,nil);
			return nil;
		}
	}
	
	[[NSData dataWithBytes:data length:dataLength] writeToFile:tiffFilePath atomically:NO];
	
	// Template kap generation
	[kapTemplate appendString:[NSString stringWithFormat:@"VER/%@\r\n",[someData BSBVersion]]];
	[kapTemplate appendString:[NSString stringWithFormat:@"BSB/NA=%@,NU=1,RA=%d,%d,DU=%d\r\n",
		[someData chartName],[[someData imageWidth] intValue],[[someData imageHeight] intValue], [[someData DPI] intValue]]];
	[kapTemplate appendString:[NSString stringWithFormat:@"KNP/SC=%d,GD=%@,PR=%@,PP=%d,PI=0.000000,SP=Unknown\r\n",
		[[someData scale] intValue],[someData geodeticDatum], [someData projection],[[someData projectionParameter] intValue]]];
	[kapTemplate appendString:[NSString stringWithFormat:@"    SK=%@,TA=90,UN=%@,SD=%@,DX=6000.0,DY=6000.0\r\n",
		[[someData skewAngle] stringValue],[someData unit],[someData soundingDatum]]];
	[kapTemplate appendString:[NSString stringWithFormat:@"OST/1\r\n"]];
	[kapTemplate appendString:[NSString stringWithFormat:@"CED/SE=%@,RE=01,ED=%@\r\n",
		[[someData editDate] description],
		[[someData editDate] description]]];
	
	NSDictionary * pointDict = [someData refPoints];
	int i = 0;
	int max = [pointDict count];
	for (; i < max; i++) {
		NSDictionary * point = [pointDict valueForKey:[NSString stringWithFormat:@"%d",i]];
		[kapTemplate appendString:[NSString stringWithFormat:@"REF/%d,%d,%d,%g,%g\r\n", i+1, [[point valueForKey:@"x"] intValue],
														[[point valueForKey:@"y"] intValue],
														[[point valueForKey:@"lat"] doubleValue],
														[[point valueForKey:@"lon"] doubleValue]]];
	}
	
//	NSArray * pointArray = [someData autoSortedPlyPoints];
	NSArray * pointArray = [someData autoMaxAreaPlyPoints];
	i = 0;
	max = [pointArray count];
	for (; i < max; i++) {
		NSDictionary * point = [pointArray objectAtIndex:i];
		[kapTemplate appendString:[NSString stringWithFormat:@"PLY/%d,%g,%g\r\n", i+1, [[point valueForKey:@"lat"] doubleValue],
													   [[point valueForKey:@"lon"] doubleValue]]];
	}
	
	// bsb file generation
	[kapTemplate writeToFile:kapTemplatePath atomically:NO];
	char * argv[4];
	argv[1] = (char *) [kapTemplatePath cString];
	argv[2] = (char *) [tiffFilePath cString];
	argv[3] = (char *) [bsbResult cString];
	tif2bsb(4,argv);
	
	// Loading the resulting bsb file (to be returned)
	NSData * result = [NSData dataWithContentsOfFile:bsbResult];
	
	// Cleanup temporary files
	[[NSFileManager defaultManager] removeFileAtPath:bsbResult handler:nil];
	[[NSFileManager defaultManager] removeFileAtPath:tiffFilePath handler:nil];
	[[NSFileManager defaultManager] removeFileAtPath:kapTemplatePath handler:nil];
	[[NSFileManager defaultManager] removeFileAtPath:tempPath handler:nil];
	return result; 
}

- (void) dealloc {
	[super dealloc];
}

@end
