//
//  ChartData.m
//  RasterChart2BSB
//
//  Created by Nicolas Cherel on 08/07/05.
//  Copyright 2005 Magic Instinct Software. 
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

//

#import "ChartData.h"

#define max(X,Y) ((X) > (Y) ? (X) : (Y))
#define min(X,Y) ((X) < (Y) ? (X) : (Y))

typedef struct {float u;float v;} vector;

//--------------------------- TABLEDATASOURCES -------------------------------//

#pragma mark -
#pragma mark TableDataSource

@interface PointsTableDataSource : NSObject {
@protected
	NSMutableDictionary		*_dictForData;
}
-(PointsTableDataSource *)initWithDictionary:(NSDictionary *)dict;
@end

@implementation PointsTableDataSource 

-(PointsTableDataSource *)initWithDictionary:(NSDictionary *)dict
{
	[super init];
	_dictForData = [dict mutableCopy];
	return self;
}

- (PointsTableDataSource *)initWithMutableDictionary:(NSMutableDictionary *)dict
{
	[super init];
	_dictForData = dict;
	return self;
}


- (int)numberOfRowsInTableView:(NSTableView *)aTableView
{
	return [_dictForData count];
}

- (id)tableView:(NSTableView *)aTableView objectValueForTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex
{
	return [[_dictForData valueForKey:[NSString stringWithFormat:@"%d",rowIndex]] valueForKey:[aTableColumn identifier]];
}

- (void)tableView:(NSTableView *)aTableView setObjectValue:(id)anObject forTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex
{
	if (anObject == nil) {
		[_dictForData removeObjectForKey:[NSString stringWithFormat:@"%d",rowIndex]];
		int total = [_dictForData count];
		int i = rowIndex;
		while (i < total) {
			[_dictForData setObject:[_dictForData objectForKey:[NSString stringWithFormat:@"%d",i+1]] forKey:[NSString stringWithFormat:@"%d",i]];
			[_dictForData removeObjectForKey:[NSString stringWithFormat:@"%d",i+1]];
			i++;
		}
	} else {
		if (rowIndex >= [_dictForData count]) {
			[_dictForData setValue:[[[NSMutableDictionary alloc] init] autorelease] forKey:[NSString stringWithFormat:@"%d",rowIndex]];
		}
		[[_dictForData valueForKey:[NSString stringWithFormat:@"%d",rowIndex]] setValue:anObject forKey:[aTableColumn identifier]];
	}
}

@end

#pragma mark -
#pragma mark PLY (Polygon describing valids area in image) TableDataSource

// Sorting fonction
int sortPoints(id obj1, id obj2, void * aCenter)
{
	assert(aCenter != NULL);
	NSPoint center = *((NSPoint *)aCenter);
	vector a, b, unit;
	a.u = [[obj1 objectForKey:@"x"] floatValue] - center.x;
	a.v = [[obj1 objectForKey:@"y"] floatValue] - center.y;
	b.u = [[obj2 objectForKey:@"x"] floatValue] - center.x;
	b.v = [[obj2 objectForKey:@"y"] floatValue] - center.y;
	unit.u = 1.;
	unit.v = 0.;
	
	float angleA = acosf ((a.u * unit.u + a.v * unit.v) / 
		(sqrtf (a.u * a.u + a.v * a.v) * sqrtf(unit.u * unit.u + unit.v * unit.v)));
	float detA = a.u * unit.v - unit.u * a.v;
	if (detA < 0) angleA = -angleA;
	
	float angleB = acosf ((b.u * unit.u + b.v * unit.v) / 
		(sqrtf (b.u * b.u + b.v * b.v) * sqrtf(unit.u * unit.u + unit.v * unit.v)));		
	float detB = b.u * unit.v - unit.u * b.v;
	if (detB < 0) angleB = -angleB;
	
	if (detB < 0 && detA >= 0) return NSOrderedDescending;
	else if (detB >= 0 && detA < 0) return NSOrderedDescending;
	else if (detB >= 0 && detA >= 0) return angleA > angleB ? NSOrderedAscending : NSOrderedDescending;
	else return angleA < angleB ? NSOrderedAscending : NSOrderedDescending;	
	/*
	if (detA >= 0 && detB >= 0) ngleB ? NSOrderedAscending : NSOrderedDescending;
	} else if (detA < 0 && detB < 0) {
		NSLog (@"o1 = (%f,%f) o2 = (%f,%f) :: %f < %f ? %@", [[obj1 objectForKey:@"x"] floatValue], [[obj1 objectForKey:@"y"] floatValue], [[obj2 objectForKey:@"x"] floatValue], [[obj2 objectForKey:@"y"] floatValue], angleA, angleB, angleA < angleB ?  @"OrderedAscending" : @"OrderedDescending");
		return angleA < angleB ? NSOrderedAscending : NSOrderedDescending;
	} else if (detA < 0) {
		NSLog (@"o1 = (%f,%f) o2 = (%f,%f) :: A : %f > pi/2 ? %@",[[obj1 objectForKey:@"x"] floatValue], [[obj1 objectForKey:@"y"] floatValue], [[obj2 objectForKey:@"x"] floatValue], [[obj2 objectForKey:@"y"] floatValue],  angleA, angleA > pi/2 ? @"OrderedAscending" : @"OrderedDescending");
		return angleA > pi/2 ? NSOrderedAscending : NSOrderedDescending;
	} else {
		NSLog (@"o1 = (%f,%f) o2 = (%f,%f) :: B : %f > pi/2 ? %@",[[obj1 objectForKey:@"x"] floatValue], [[obj1 objectForKey:@"y"] floatValue], [[obj2 objectForKey:@"x"] floatValue], [[obj2 objectForKey:@"y"] floatValue],  angleB, angleB > pi/2 ? @"OrderedAscending" : @"OrderedDescending");
		return angleB > pi/2 ? NSOrderedAscending : NSOrderedDescending;
	}
	*/
}

@interface PLYPointsTableDataSource : PointsTableDataSource
-(NSArray *)sortedValues;
@end

@implementation PLYPointsTableDataSource

-(NSArray *)sortedValues;
{
	NSMutableArray * sortedArray = [NSMutableArray arrayWithArray:[_dictForData allValues]];
	NSEnumerator * enu = [sortedArray objectEnumerator];
	NSPoint c = NSMakePoint(0,0);
	int iterationCount = 1;
	id obj;
	while (obj = [enu nextObject]) {
		NSPoint curr = NSMakePoint([[obj objectForKey:@"x"] floatValue],[[obj objectForKey:@"y"] floatValue]);
		c.x = c.x + (curr.x - c.x) / iterationCount;		
		c.y = c.y + (curr.y - c.y) / iterationCount;
		iterationCount++;
	}
	[sortedArray sortedArrayUsingFunction:sortPoints context:&c];

	return sortedArray;
}

@end



//-------------------------- END TABLEDATASOURCE ----------------------------//

#pragma mark -
#pragma mark ChartData

@implementation ChartData
#pragma mark init

- (ChartData *)init
{
	[super init];
	_dictForData = [[NSMutableDictionary alloc] init];
	[_dictForData setValue:[[[NSMutableDictionary alloc] init] autorelease] forKey:@"ref points"];
	[_dictForData setValue:[[[NSMutableDictionary alloc] init] autorelease] forKey:@"ply points"];
	return self;
}

- (ChartData *)initWithDictionary:(NSDictionary *)dict
{
	[super init];
	_dictForData = [dict mutableCopy];
	return self;
}

#pragma mark DataSources accessors
- (id)refPointsTableDataSource
{
	return [[PointsTableDataSource alloc] initWithMutableDictionary:[_dictForData valueForKey:@"ref points"]];
}

- (id)plyPointsTableDataSource
{
	return [[PLYPointsTableDataSource alloc] initWithMutableDictionary:[_dictForData valueForKey:@"ply points"]];
}

#pragma mark Setters
- (void)setBSBVersion:(NSString *)aVer
{
	[_dictForData setValue:aVer forKey:@"version"];
}

- (void)setImageWidth:(NSNumber *)aWidth
{
	[_dictForData setValue:aWidth forKey:@"width"];
}

- (void)setImageHeight:(NSNumber *)aHeight
{
	[_dictForData setValue:aHeight forKey:@"height"];
}

- (void)setChartName:(NSString *)aName
{
	[_dictForData setValue:aName forKey:@"name"];
}

- (void)setDPI:(NSNumber *)aDPI
{
	[_dictForData setValue:aDPI forKey:@"DPI"];
}

- (void)setEditDate:(NSDate *)aDate
{
	[_dictForData setValue:aDate forKey:@"edit date"];
}

- (void)setGeodeticDatum:(NSString *)aDatum
{
	[_dictForData setValue:aDatum forKey:@"geodetic datum"];
}

- (void)setProjection:(NSString *)aProjectionName
{
	[_dictForData setValue:aProjectionName forKey:@"projection"];
}

- (void)setProjectionParameter:(NSNumber *)aProjectionParameter
{
	[_dictForData setValue:aProjectionParameter forKey:@"projection parameter"];
}

- (void)setScale:(NSNumber *)aScale
{
	[_dictForData setValue:aScale forKey:@"scale"];
}

- (void)setSkewAngle:(NSNumber *)aSkewAngle
{
	[_dictForData setValue:aSkewAngle forKey:@"skew angle"];
}

- (void)setSoundingDatum:(NSString *)aSoundingDatum
{
	[_dictForData setValue:aSoundingDatum forKey:@"sounding datum"];
}

- (void)setUnit:(NSString *)aUnit
{
	[_dictForData setValue:aUnit forKey:@"unit"];
}

#pragma mark Getters
- (NSString *)BSBVersion
{
	return [_dictForData valueForKey:@"version"];
}

- (NSNumber *)imageWidth
{
	return [_dictForData valueForKey:@"width"];
}

- (NSNumber *)imageHeight
{
	return [_dictForData valueForKey:@"height"];
}

- (NSString *)chartName
{
	return [_dictForData valueForKey:@"name"];
}

- (NSNumber *)DPI
{
	return [_dictForData valueForKey:@"DPI"];
}

- (NSCalendarDate *)editDate
{
	return [_dictForData valueForKey:@"edit date"];
}

- (NSString *)geodeticDatum
{
	return [_dictForData valueForKey:@"geodetic datum"];
}

- (NSString *)projection
{
	return [_dictForData valueForKey:@"projection"];
}

- (NSNumber *)projectionParameter
{
	return [_dictForData valueForKey:@"projection parameter"];
}

- (NSNumber *)scale
{
	return [_dictForData valueForKey:@"scale"];
}

- (NSNumber *)skewAngle
{
	return [_dictForData valueForKey:@"skew angle"];
}

- (NSString *)soundingDatum
{
	return [_dictForData valueForKey:@"sounding datum"];
}

- (NSString *)unit
{
	return [_dictForData valueForKey:@"unit"];
}

- (NSDictionary *)refPoints
{
	return [_dictForData valueForKey:@"ref points"];
}

- (NSDictionary *)plyPoints
{
	return [_dictForData valueForKey:@"ply points"];
}

- (NSArray *)autoSortedPlyPoints
{
	PLYPointsTableDataSource * tableDataSource = [[[PLYPointsTableDataSource alloc] initWithDictionary:[self refPoints]] autorelease];
	return [tableDataSource sortedValues];
}

- (NSArray *)autoMaxAreaPlyPoints
{
	// We should use all the points to get a better precision : here, we use
	// two first ones.
	NSDictionary * refs = [self refPoints];
	NSDictionary * point1 = [refs objectForKey:@"0"];
	NSDictionary * point2 = [refs objectForKey:@"1"];
	// Getting the values
	float x1 = [[point1 objectForKey:@"x"] floatValue],
		y1 = [[point1 objectForKey:@"y"] floatValue],
		lat1 = [[point1 objectForKey:@"lat"] floatValue],
		lon1 = [[point1 objectForKey:@"lon"] floatValue],
		x2 = [[point2 objectForKey:@"x"] floatValue],
		y2 = [[point2 objectForKey:@"y"] floatValue],
		lat2 = [[point2 objectForKey:@"lat"] floatValue],
		lon2 = [[point2 objectForKey:@"lon"] floatValue];
		
	// We test if all coordinates between point1 and 2 are differents, 
	// If it is not the case we search another point, if no points have
	// its two coordinates differents, we try with another start point.
	int startPoint = 0;
	int secPoint = 1;
	while (((fabsf(x1 - x2) < 10) || (fabsf(y1 - y2) < 10)) && (startPoint < [refs count])) {
		secPoint++;
		if (secPoint == [refs count] ){
			secPoint = 0;
			
			startPoint++;
			point1 = [refs objectForKey:[NSString stringWithFormat:@"%d",startPoint]];
			x1 = [[point1 objectForKey:@"x"] floatValue];
			y1 = [[point1 objectForKey:@"y"] floatValue];
			lat1 = [[point1 objectForKey:@"lat"] floatValue];
			lon1 = [[point1 objectForKey:@"lon"] floatValue];
		}
		point2 = [refs objectForKey:[NSString stringWithFormat:@"%d",secPoint]];
		x2 = [[point2 objectForKey:@"x"] floatValue];
		y2 = [[point2 objectForKey:@"y"] floatValue];
		lat2 = [[point2 objectForKey:@"lat"] floatValue];
		lon2 = [[point2 objectForKey:@"lon"] floatValue];
	}
	
	// We must solve the equation :
	float lonPerPx = (lon1 - lon2) / (x1 - x2);
	float latPerPx = (lat1 - lat2) / (y1 - y2);
	float lonDecay = ((lon1 - x1 * lonPerPx) + (lon2 - x2 * lonPerPx)) / 2;
	float latDecay = ((lat1 - y1 * latPerPx) + (lat2 - y2 * latPerPx)) / 2;
	int width = [[self imageWidth] intValue];
	int height = [[self imageHeight] intValue];
	
	NSArray * array = [NSArray arrayWithObjects:
		[NSMutableDictionary dictionaryWithObjectsAndKeys:
			[NSNumber numberWithFloat:lonDecay],@"lon",
			[NSNumber numberWithFloat:latDecay],@"lat",nil],
		[NSMutableDictionary dictionaryWithObjectsAndKeys:
			[NSNumber numberWithFloat:lonDecay],@"lon",
			[NSNumber numberWithFloat:(height * latPerPx) + latDecay],@"lat",nil],
		[NSMutableDictionary dictionaryWithObjectsAndKeys:
			[NSNumber numberWithFloat:(width * lonPerPx) + lonDecay],@"lon",
			[NSNumber numberWithFloat:(height * latPerPx) + latDecay],@"lat",nil],
		[NSMutableDictionary dictionaryWithObjectsAndKeys:
			[NSNumber numberWithFloat:(width * lonPerPx) + lonDecay],@"lon",
			[NSNumber numberWithFloat:latDecay],@"lat",nil],
	nil];
	NSEnumerator * enu = [array objectEnumerator];
	id obj;
	while (obj = [enu nextObject]) {
		float lat = [[obj valueForKey:@"lat"] floatValue];
		if ( lat > 90) {
			lat = 90;
			[obj setObject:[NSNumber numberWithFloat:lat] forKey:@"lat"];
		} else if (lat < -90) {
			lat = -90;
			[obj setObject:[NSNumber numberWithFloat:lat] forKey:@"lat"];
		}
		float lon = [[obj valueForKey:@"lon"] floatValue];
		if ( lon > 180) {
			lon = 180;
			[obj setObject:[NSNumber numberWithFloat:lon] forKey:@"lon"];
		} else if (lon < -180) {
			lon = -180;
			[obj setObject:[NSNumber numberWithFloat:lon] forKey:@"lon"];
		}
	}
	return array;
}

- (void)dealloc
{
	[_dictForData release];
	[super dealloc];
}

@end
